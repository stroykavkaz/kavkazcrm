<?php

require __DIR__ . '/vendor/autoload.php';

spl_autoload_register(function ($classname) {
	$file = __DIR__ . "/src/controllers/" . strtolower($classname) . ".php";
	if (file_exists($file)){
		require $file;
	}
});

require 'src/_const.php'; // константы
require __DIR__ . '/src/controllers/AbstractController.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/src/settings.php';
$app = new \Slim\App($settings);

// // Set up dependencies

require __DIR__ . '/src/dependencies.php';
require __DIR__ . '/src/login.php';

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

$app->add(new \Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware);

// // Register middleware
require __DIR__ . '/src/middleware.php';

require __DIR__ . '/src/routes.php';



// throw new Exception("Something broke!");
// Run app
$app->run();

<?php

function getUserLevel()
{
    global $app;
    $db = $app->getContainer()->get('db');

    $user_id = getUserId();//decrypt($_COOKIE['token']);

    $db->where('user_id', $user_id);
    $user_level = $db->getValue('user', 'user_level');

    return $user_level;

}

function getUserId()
{
    $user_id = decrypt($_COOKIE['token']);

    return $user_id;

}

function uuid()
{
    if (function_exists('com_create_guid')) {
        return com_create_guid();
    } else {
        mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
        $charid = md5(uniqid(rand(), TRUE));
        $hyphen = chr(45);// "-"
        $uuid =
            substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);

        return $uuid;
    }
}

    function encrypt($str)
    {
        $key = 'PASSPHRASE';
        for ($i = 0; $i < strlen($str); $i++) {
            $char = substr($str, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keychar));
            @$result .= $char;
        }

        return (base64_encode($result));
    }


    function decrypt($str)
    {
        $str = base64_decode(($str));
        $result = '';
        $key = 'PASSPHRASE';
        for ($i = 0; $i < strlen($str); $i++) {
            $char = substr($str, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result .= $char;
        }

        return $result;
    }

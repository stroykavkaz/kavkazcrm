<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Fias extends AbstractController{

	public function get ($request, $response, $args) {
		$params = $request->getParams();
		if (array_key_exists('city', $params))
			if (array_key_exists('street', $params)) $q = 'SELECT * FROM d_fias_addrobj WHERE  aolevel = 7 AND actstatus = 1 AND parentguid = \''.$params['city'].'\' AND formalname LIKE \'%'.$params['street'].'%\' LIMIT 10';
			else $q = 'SELECT * FROM d_fias_addrobj WHERE   streetcode = \'0000\' AND aolevel < 7 AND  actstatus = 1 AND  formalname LIKE \''.$params['city'].'%\' ORDER BY aolevel LIMIT 5';
		else if (array_key_exists('street_uid', $params)) {
			$q = 'SELECT * FROM `d_fias_houseint` WHERE `aoguid` = \''.$params['street_uid'].'\'';
		}
		else {
			return $response->withJson(['success'=>false]);
		}

		$data = $this->db->rawQuery($q);
		$result = $this->result($data);

		return $response->withJson($result);
	}

}

<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Stats extends AbstractController
{

    public function get($request, $response, $args) {

        $orderby_field=($request->getParam('orderby_field')) ? $request->getParam('orderby_field') : 'total';
        $orderby_dir=($request->getParam('orderby_dir')) ? $request->getParam('orderby_dir') : 'DESC';
        $order = "ORDER BY $orderby_field $orderby_dir";

        $between = '';
//        print_r($request);
        if ($request->getParam('d1')) {
           $between = "AND action.datetime1 >= '".$request->getParam('d1')."' AND  action.datetime1 <= '".$request->getParam('d2')."'";
        }

        $q = "select * from (select  action.user_id id, user.name,  user.surname, count(action.user_id) total
from action, user 
where  action.hide=0 AND user.user_id = action.user_id $between
 group by  action.user_id
order by  total DESC) as A

left JOIN 
 (select count(action.user_id) cold, action.user_id id2
from action
where action.hide=0 AND action.action_type_id<5 $between
 group by  action.user_id
) as B

ON A.id = B.id2


left JOIN 
 (select count(action.user_id) deal, action.user_id id2
from action
where action.hide=0 AND action.action_type_id=5 $between
group by  action.user_id
) as C

ON A.id = C.id2

left JOIN 
 (select count(action.user_id) refuse, action.user_id id2
from action
where action.hide=0 AND action.action_type_id>5 $between
group by  action.user_id
) as D

ON A.id = D.id2 
$order";


        $stats = $this->db->rawQuery($q);

        $out = $this->result($stats,'stats');

        return $response->withJson($out);


    }


    public function user ($request, $response, $args) {

        $user_id = $request->getParam('user_id');
        $date1 = $request->getParam('date1');
        $date2 = $request->getParam('date2');
        $where_date="";
        if($date1) $where_date = "AND datetime1 >='$date1' AND datetime1 <= '$date2' "; // если дата не указана, то выводим за всё время
        $where_user = " AND action.user_id = $user_id";


       $q = "SELECT txt, action.datetime1,action_type_id,
		DATE_FORMAT(action.future,'%d-%m-%y') future,action_id,
		org.name_short, org.org_id, action.contact_person, user.name
		FROM org, user, action
		WHERE action.org_id=org.org_id AND action.hide=0
		AND user.user_id =action.user_id
		$where_date $where_user 
		ORDER BY action.datetime1 DESC";

        $actions = $this->db->rawQuery($q);
        $out = $this->result($actions,'user');

     return $response->withJson($out);
    }



}

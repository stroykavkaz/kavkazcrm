<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Dep extends AbstractController{

	public function get_item ($request, $response, $args) {

		$this->db->orderBy('dep_id', 'ASC');
		$this->db->where('hide', 0);
		// $db->where('dep.dep_id=dep_org.dep_id');
		$dep = $this->db->get('dep');

		$out = $this->result($dep,'deps');

		return $response->withJson($out);
	}

	public function get ($request, $response, $args) {
		$q = 'SELECT dep.dep, dep.dep_id, dep.group,
 count(dep_org.dep_id) count
		FROM dep
		LEFT JOIN dep_org ON dep.dep_id= dep_org.dep_id
		WHERE dep.hide=0
		GROUP BY dep.dep_id
		ORDER BY dep.group, dep.dep';

		$dep = $this->db->map('dep_id')->ArrayBuilder()->rawQuery($q);
//        $db->map('org_id')->ArrayBuilder()
		$out = $this->result($dep,'deps');

		return $response->withJson($out);
	}

	public function add ($request, $response, $args) {
        $data = array('dep' => $request->getParam('dep'),'group' => $request->getParam('group'),);

        $id = $this->db->insert('dep', $data);
		$out = $this->result($id,'insert_id');
		return $response->withJson($out);
	}

	public function edit_item ($request, $response, $args) {

		// $hide = (int)($request->getParam('delete') == 'on');
		$hide = $request->getParam('hide') ? $request->getParam('hide') : 0;

		$data = array(
			'dep' => $request->getParam('dep'),
			'hide' => $hide,
		);

		$this->db->where('dep_id', $args['id']);
		$update_result  = $this->db->update('dep', $data, 1);

		$out = $this->result($update_result,'update_result');
		return $response->withJson($out);
	}
}

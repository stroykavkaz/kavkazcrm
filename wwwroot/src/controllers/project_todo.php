<?php


class Project_todo extends AbstractController
{
    public function get($request, $response, $args)
    {
        $level = $request->getQueryParam('level', $default = 2);
        $b_date = $request->getQueryParam('b_date', $default = 0);
        $e_date = $request->getQueryParam('e_date', $default = 0);
        $project = $request->getQueryParam('project_id', $default = -1);
        $user_id = $request->getQueryParam('user_id', $default = -1);
        $user_id = !$user_id ? getUserId() : $user_id;
        $parent_id = $request->getQueryParam('parent_id', $default = 0);
        $todos = $this->getToDo(-1, $level, $project, $user_id, $parent_id, $b_date, $e_date);

        $result = $this->result($todos);

        return $response->withJson($result);
    }

    public function get_item($request, $response, $args)
    {
        $level = $request->getQueryParam('level', $default = 2);
        $project = $request->getQueryParam('project_id', $default = -1);
        $user_id = $request->getQueryParam('user_id', $default = -1);
        $todos = $this->getToDo($args['id'], $level, $project, $user_id, -1);
        $result = $this->result($todos);

        return $response->withJson($result);
    }
    public function edit_item($request, $response, $args)
    {
    $arr = array(
    'title' => $request->getParam('title'),
    'info' => $request->getParam('info'),
    'hours_estimate' => $request->getParam('hours_estimate'),
    'hours_spent' => $request->getParam('hours_spent'),
      );

      if($request->getParam('date_start')) $arr['date_start'] = $request->getParam('date_start');
      if($request->getParam('deadline')) $arr['deadline'] = $request->getParam('deadline');


        $user_ids = $request->getParam('user_ids', []);
        $arr['user_ids'] = ' '.implode(' ', $user_ids).' |';

        $this->db->where('project_todo_id', $args['id']);
        $sql_result = $this->db->update('project_todo', $arr);
        $result = $this->result($sql_result,'status');

        $this->db->where('project_todo_id', $args['id']);
        $todo = $this->getToDo($args['id'], 2);
        $result = $this->result($todo[$args['id']],'data',$result);
        return $response->withJson($result);
    }

    public function add($request, $response, $args)
    {
        $data = array();
        $data['title'] = $request->getParam('title', $default = 'Название этапа');
        $data['info'] = $request->getParam('info', $default = 'Название этапа');
        $data['project_id'] = $request->getParam('project_id', $default = 0);
        $data['parent_id'] = $request->getParam('parent_id', $default = 0);
        if ($request->getParam('deadline')) $data['deadline'] = $request->getParam('deadline');
        if ($request->getParam('date_start')) $data['date_start'] = $request->getParam('date_start');
        if ($request->getParam('hours_estimate')) $data['hours_estimate'] = $request->getParam('hours_estimate');
        if ($request->getParam('hours_spent')) $data['hours_spent'] = $request->getParam('hours_spent');

        // $data['date_start'] = $request->getParam('date_start') ? $request->getParam('date_start') : '';
        // $data['hours_estimate'] = $request->getParam('hours_estimate', $default = 0);
        // $data['hours_spent'] = $request->getParam('hours_spent', $default = 0);

        $user_ids = $request->getParam('user_ids', []);
        if ($user_ids) {
            $data['user_ids'] = ' '.implode(' ', $user_ids).' |';
        }

        $id = $this->db->insert('project_todo', $data);

        $item = $this->getToDo($id);

        $event = array('event_type' => 'added', 'event' => 'Этап проекта создан', 'user_id' => getUserId(), 'item_id' => $data['project_id']);
        $this->db->insert('project_history', $event);

        $result = $this->result($id, 'insert_id');
        $result = $this->result($item, 'item', $result);

        return $response->withJson($result);
    }

    public function delete_item($request, $response, $args)
    {
        $this->db->where('project_todo_id', $args['id']);
        $delete = $this->db->update('project_todo', array('deleted' => '1'));

        $this->db->where('project_todo_id', $args['id']);
        $project_todo = $this->db->get('project_todo');

        $result = $this->result($delete);

        $event = array('event_type' => 'deleted', 'event' => 'Этап проекта удален', 'user_id' => getUserId(), 'item_id' => $project_todo[0]['project_id']);
        $this->db->insert('project_history', $event);

        $result = $this->result(new DateTime(), 'accessTime', $result);

        return $response->withJson($result);
    }

    private function getToDo($id, $level = 1,  $project = -1, $user_id = -1, $parent = -1, $b_date = 0, $e_date = 0)
    {
        if ($level <= 0) {
            return new ArrayObject();
        }
        $level = $level - 1;
        $q = 'select project_todo_id, title, info, deadline, created, org_id, project_status_id, parent_id, project_id, user_ids from  project_todo';
        $this->db->where('deleted', 0);
        if ($id > -1) {
            $this->db->where('project_todo_id', $id);
        }
        if ($parent > -1) {
            $this->db->where('parent_id', $parent);
        }
        if ($project > -1) {
            $this->db->where('project_id', $project);
        }
        if ($user_id > 0) {
            $this->db->where("user_ids LIKE '% ".$user_id." %'");
        }
        if ($e_date) {
            $this->db->where('date_start', $e_date, '<=');
        }
        if ($b_date) {
            $this->db->where('deadline', $b_date, '>=');
        }
        $todos = $this->db->map('project_todo_id')->ArrayBuilder()->get('project_todo');

        foreach ($todos as $key => $value) {

            $arr_user_ids=array_filter(explode(' ', trim($todos[$key]['user_ids'])), function ($value) {
                return $value !== '' && $value !== '|';
            });
            array_walk($arr_user_ids,function (&$v){ $v= intval($v);});


            $todos[$key]['user_ids'] = $arr_user_ids;

            $todos[$key]['user_ids'] = $todos[$key]['user_ids'] ? $todos[$key]['user_ids'] : [];

            $sub_todos = $this->getToDo(-1, $level, -1, -1, $todos[$key]['project_todo_id']);
            $todos[$key]['todos'] = $sub_todos ? $sub_todos : new ArrayObject();
        }

        return $todos;
    }

    public function update_status_item($request, $response, $args)
    {
        $status = $request->getParam('status', $default = [-1]);
        if ($status >= 0) {
            $status_list = $this->db->map('project_status_id')->ArrayBuilder()->get('project_status');
            $this->db->where('project_todo_id', $args['id']);
            $project_todo = $this->db->get('project_todo');
            $this->db->where('project_todo_id', $args['id']);
            $update = $this->db->update('project_todo', array('project_status_id' => $status));

            $event = array('event_type' => 'status_changed', 'event' => 'Статус этапа '.$project_todo[0]['title'].' изменен с '.$status_list[$project_todo[0]['project_status_id']].' на '.$status_list[$status], 'user_id' => getUserId(), 'item_id' => $args['id']);
            $this->db->insert('project_history', $event);

            $result = $this->result($status);

            return $response->withJson($result);
        }

        return $response->withJson(['status' => 'false']);
    }
}

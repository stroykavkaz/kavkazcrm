<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Action extends AbstractController{

	public function get ($request, $response, $args) {
		$current_user_id = getUserId();

		if ($project_todo_id = $request->getParam('project_todo_id')){
			//DATE_FORMAT(action.datetime1,'%d-%m-%y %H:%i')

			$cols = Array ("action.project_todo_id","txt","status", "datetime1", "future","action_id","org.name_short", "org.org_id", "action.contact_person", "t.label");

//			$this->db->join('task t','t.task_id=action.task_id','LEFT');
			$this->db->join('org','action.org_id=org.org_id','LEFT');
			$this->db->where('action.hide',0);

			if ($project_todo_id >= 0) $this->db->where('project_todo_id',$project_todo_id);

			$this->db->where('future',date("Y-m-d"),'>=');
			$this->db->where('action.user_id',$current_user_id);
			$this->db->orderBy("action.datetime1","desc");

			$actions = $this->db->get('action', null, $cols);
			$result = $this->result($actions);
			$result = $this->result($project_todo_id,'project_todo_id',$result);
			return $response->withJson($result);
		}
		$param = $request->getParam('month');

		if ($param) {
			list($month, $year) = explode('-', $param);
			$where_date = "AND datetime1 >='$year-$month-01' AND datetime1 <= DATE_ADD('$year-$month-01', INTERVAL 1 MONTH)";


		} else {
			$where_date = "AND a.datetime1 >=DATE_FORMAT(NOW(), '%Y/%m/1')";
		}

		// DATE_FORMAT(a.datetime1,'%d-%m-%y %H:%i') datetime1,
		$q = "SELECT txt, datetime1,
		future,action_id,
		org.name_short, org.org_id, a.contact_person, t.label
		FROM org,action a
		LEFT JOIN task t on t.task_id=a.task_id
		WHERE a.org_id=org.org_id AND a.hide=0 $where_date and a.user_id=$current_user_id
		ORDER BY a.datetime1 DESC";

		$actions = $this->db->rawQuery($q);

		$out = $this->result($actions,'actions');

		return $response->withJson($out);
	}

	public function get_item ($request, $response, $args) {

        if(JAVADB) {
            $q = "SELECT tab_action.theme txt, tab_action.id action_id,
tab_client.name name_short, tab_action.adopted contact_person, tab_action.actiontype action_type_id,
	actiondt datetime1,	futuredt future,tab_client.id org_id
		FROM  tab_client, tab_action		
		WHERE tab_action.id_client=tab_client.id and tab_action.id=?";
        }else{
            $q = "SELECT txt, action_id,org.name_short, a.contact_person,action_type_id,
	datetime1,	future, org.name_short, org.org_id, a.task_id
		FROM  org, action a		
		WHERE a.org_id=org.org_id and a.action_id=?";
        }



		$action =  $this->db->rawQuery($q,[$args['action_id']]);


		return $response->withJson($action[0]);
	}




	public function add ($request, $response, $args) {

		$arr = array('txt' => $request->getParam('txt'),
							 'action_type_id' => $request->getParam('action_type_id'),
							 'contact_person' => $request->getParam('contact_person'),
								'org_id' => isset($args['firm_id']) ? $args['firm_id'] : $request->getParam('org_id'),
								'user_id' =>getUserId(),
							 'datetime1' => date('Y-m-d H:i:s'),
		);


		if($request->getParam('future'))
		    $arr['future']=$request->getParam('future');
		else
            $arr['future']='NULL';

              if(JAVADB) {
$q = "INSERT INTO tab_action SET theme='$arr[txt]',actiontype=$arr[action_type_id], 
person = '$arr[contact_person]', futuredt='$arr[future]',
id_client = $arr[org_id], id_user = $arr[user_id], 
un_user=(SELECT un_user FROM tab_user WHERE id=$arr[user_id]),
guid_client=(SELECT guid_client FROM tab_client WHERE id=$arr[org_id]),
guid_action=uuid(),actiondt = '$arr[datetime1]', tmst = '$arr[datetime1]'";
            $res = $this->db->rawQuery($q);
                  $insert_id = $this->db->rawQuery('SELECT LAST_INSERT_ID() lastid;');
                  $result = $insert_id[0]['lastid'];

                  // обновляем счетчик в org (раньше был медленный подзапрос)
                  $org_id = $request->getParam('org_id');
                  $q = "UPDATE tab_client SET action_last = NOW(), action_next='$arr[future]',
action_count=( SELECT count(id) FROM tab_action	WHERE tab_action.id_client ='$org_id')
 WHERE id='$org_id'";
                  $res = $this->db->rawQuery($q);

        }
        else {
            $id = $this->db->insert('action', $arr);
            $result = $this->result($id,'insert_id');
            if ($id) {
                $q = "SELECT action.*, user.name, user.surname from action, user WHERE action.user_id=user.user_id and action.action_id=$id";
                $data = $this->db->rawQuery($q);
                $result = $this->result($data[0],'data',$result);

                // обновляем счетчик в org (раньше был медленный подзапрос)
                $org_id = $request->getParam('org_id');
                $q = "UPDATE org SET action_last = NOW(), action_next='$arr[future]',
action_count=( SELECT count(action_id) FROM action	WHERE hide=0 AND action.org_id ='$org_id')
 WHERE org_id='$org_id'";
                $res = $this->db->rawQuery($q);
            }
        }
	return $response->withJson($result);
	}




	public function edit_item ($request, $response, $args) {


        if(JAVADB) {
            $arr = array(
                'theme' => $request->getParam('txt'),
                'person' => $request->getParam('contact_person'),
                'actiontype' => $request->getParam('action_type_id'),
                'futuredt' => $request->getParam('future'),

            );

            $this->db->where('id', $args['action_id']);
            $id = $this->db->update('tab_action', $arr);
            $out = $this->result($id,'insert_id');

        }
        else{
            if($request->getParam('hide')=='true') $hide = 1; else  $hide = 0;

            $arr = array(
                'txt' => $request->getParam('txt'),
                'contact_person' => $request->getParam('contact_person'),
                'action_type_id' => $request->getParam('action_type_id'),
                'hide' => $hide,
            );

//           if($request->getParam('action_type_id')) $arr['action_type_id']=$request->getParam('action_type_id');
            if($request->getParam('future')) $arr['future']=$request->getParam('future');

            $this->db->where('action_id', $args['action_id']);
            $id = $this->db->update('action', $arr);
            $out = $this->result($id,'insert_id');
        }


		return $response->withJson($out);
	}

	public function show ($request, $response, $args) {
		$current_user_id = getUserId();
		if ($args['month_year']){
			$user_id = $args['user_id'];
			$task_id = $args['task_id'];
			$month_year = $args['month_year'];
		}else{
			$user_id = 2;
			$month_year = $request->getParam('month');
			$task_id = '0';
		}

		if ($month_year == 'all_months') {
			$where_date = '';
		} elseif ($month_year == 'cur_month') {
			$where_date = "AND action.datetime1 >=DATE_FORMAT(NOW(), '%Y/%m/1')"; // текущий месяц
		} else { // конкретный месяц, месяц передаю так '7-2016'
			list($year,$month,$day ) = explode('-', $month_year);
			$where_date = "AND datetime1 >='$year-$month-01' AND datetime1 <= DATE_ADD('$year-$month-01', INTERVAL 1 MONTH)";
		}
		$user_id = $user_id == 0 ? getUserId() : $user_id;
		if ($user_id != 'all_users') {
			$where_user = " AND action.user_id = $user_id";
		} else {
			$where_user = '';
		}

		if ($task_id == '0' OR $task_id == 'all_tasks') {
			$where_task = "";
		}
		else {
			$where_task = " AND action.task_id = $task_id";
		}

		$q = "SELECT txt, status, DATE_FORMAT(action.datetime1,'%d-%m-%y %H:%i') datetime1,
		DATE_FORMAT(action.future,'%d-%m-%y') future,action_id,
		org.name_short, org.org_id, action.contact_person, user.name, task.label
		FROM org, user, action
		LEFT JOIN task  on task.task_id=action.task_id
		WHERE action.org_id=org.org_id AND action.hide=0
		AND user.user_id =action.user_id
		$where_date $where_user $where_task
		ORDER BY action.datetime1 DESC";

		$actions = $this->db->rawQuery($q);

		$this->db->where('hide', 0);
		$deps = $this->db->get('dep');
		$users = $this->db->get('user');

		$this->db->where('hide', 0);
		$tasks = $this->db->get('task');

		$out = $this->result($actions,'actions');
		$out = $this->result($deps,'deps',$out);
		$out = $this->result($users,'users',$out);
		$out = $this->result($tasks,'tasks',$out);

		return $response->withJson($out);
	}

    public function action_statuses ($request, $response, $args) {


        $actions = $this->db->get('action_type');
        $out = $this->result($actions,'actions');

        return $response->withJson($out);
    }

}

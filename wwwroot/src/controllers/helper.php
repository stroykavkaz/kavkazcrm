<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Helper extends AbstractController{

    public function citySearch ($request, $response, $args) {
        // return $response->write("Hello " . $args['firm_id']);

        $db = $this->db;

        // TODO: CASE insensitive LIKE!

        $db->where('country_id', $args['country_id']);
        $db->where('city', "${args['search_str']}%", 'LIKE');

        $db->orderBy('city', 'ASC');
        $cities = $db->setQueryOption('DISTINCT')->get('city', 999, ['city',]); //'region','telcode'

        // foreach ($firms as $key => $firm1) {
        //     $firms[$key]['goto_url'] = '#firms/'.$firm1['org_id'].'/item';
        // }

        $arrout['cities'] = $cities;
        $arrout['db_debug'] = $db->trace;

        return $response->withJson($arrout);
    }
}
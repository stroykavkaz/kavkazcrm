<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Task extends AbstractController{

	public function get ($request, $response, $args) {
		$q = 'SELECT DATE_FORMAT(created,"%d-%m-%y") created, DATE_FORMAT(deadline,"%d-%m-%y") deadline,  task.task_id, label, info, COUNT(action.task_id) `action_count`,task.hide
			FROM task
			LEFT JOIN action ON action.task_id= task.task_id
			-- WHERE task.hide=0
			GROUP BY task.task_id
			ORDER BY task.hide, task_id DESC';
		$tasks = $this->db->rawQuery($q);

		$out = $this->result($tasks,'tasks');

		return $response->withJson($out);
	}

	public function add ($request, $response, $args) {
		

		$data = array(
		'label' => $request->getParam('label'),
		'info' => $request->getParam('info'),
		'deadline' => $request->getParam('deadline'),
		);
		$id = $db->insert('task', $data);

		if ($db->getLastErrno() === 0) {
		$arr['success'] = true;
		$arr['insert_id'] = $id;
		} else {
		$arr['success'] = false;
		$arr['errinfo'] = $db->getLastError();
		}

		return $response->withJson($arr);
	}

	public function edit_item ($request, $response, $args) {
		

		if ($request->getParam('delete') == 'on') {
			$hide = 1;
		} else {
			$hide = 0;
		}

		$data = array(
		'label' => $request->getParam('label'),
		'info' => $request->getParam('info'),
		// 'deadline' => $request->getParam('deadline'),
		'hide' => $hide,
		);

		if($request->getParam('deadline')) $data['deadline']=$request->getParam('deadline');

		$db->where('task_id', $args['id']);
		
		if ($db->update('task', $data, 1)) {
		$arr['success'] = true;
		} else {
		$arr['success'] = false;
		$arr['errinfo'] = $db->getLastError();
		}

		return $response->withJson($arr);
	}


}
<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

use Ramsey\Uuid\Uuid;



class Firm extends AbstractController{

// поиск организации
	public function full_search($request, $response, $args)
	{


        $q_users="	SELECT org.user_id, COUNT(org.user_id) count, user.name, user.surname
		FROM org, user
		WHERE user.user_id=org.user_id
		GROUP BY org.user_id";

        $q_cities="	SELECT city
		FROM org
		WHERE city<>''
		GROUP BY city;";

     if(JAVADB) {
         $q_users = "SELECT id user_id, name, surname
		FROM tab_user
		ORDER BY surname";

         $q_cities = "	SELECT city
		FROM tab_client
		WHERE city<>''
		GROUP BY city;";
     }
        $users = $this->db->rawQuery($q_users);
		$cities = $this->db->rawQuery($q_cities);


//		$q_statuses="	SELECT org.status_id, org_status, COUNT(org.status_id) count
//		FROM org,  org_status
//		WHERE org_status.status_id=org.status_id
//		GROUP BY  status_id;";

        $q_statuses="SELECT org_status	FROM  org_status";
		$statuses = $this->db->rawQuery($q_statuses);

		$orderby_field=($request->getParam('orderby_field')) ? $request->getParam('orderby_field') : '';
		$orderby_dir=($request->getParam('orderby_dir')) ? $request->getParam('orderby_dir') : '';

// HACK
		if($orderby_field == 'user_id') $orderby_field='org1.user_id';

        if(JAVADB) {
            if($request->getParam('search_str')){
                $search_str=$request->getParam('search_str');
                $search_str_q = "org1.name LIKE '%$search_str%' OR
			org1.phone LIKE '%$search_str%' OR
			org1.persons LIKE '%$search_str%' OR
			org1.info LIKE '%$search_str%' OR
			address LIKE '%$search_str%'";

            }else {
                $search_str_q="1=1";
            }
        }
        else{
            if($request->getParam('search_str')){
                $search_str=$request->getParam('search_str');
                $search_str_q = "name_short LIKE '%$search_str%' OR
			org1.phone1 LIKE '%$search_str%' OR
			org1.person1 LIKE '%$search_str%' OR
			org1.info LIKE '%$search_str%' OR
			org1._other LIKE '%$search_str%' OR
			address_fact LIKE '%$search_str%' OR
			(SELECT GROUP_CONCAT(phone, ' ',email,' ',person SEPARATOR '<br>') FROM org_contact WHERE org_contact.org_id = org1.org_id) LIKE '%$search_str%'";

            }else {
                $search_str_q="1=1";
            }
        }





		if($request->getParam('search_city')){
			$search_city = "city='".$request->getParam('search_city')."'";
		} else {
			$search_city='1=1';
		}

		if($request->getParam('user_id')){
			if ($request->getParam('user_id') == -1){
				$user_id = "(org1.user_id='".getUserId()."'";

                if(JAVADB) $user_id = "(org1.id_user='".getUserId()."'";
			// отключил множестенные департаменты
//				$this->db->where('user_id', getUserId());
//				$deps = $this->db->get('dep_user');
//
//				for ($i=0; $i < count($deps); $i++) {
//					$user_id .= " OR dep_org.dep_id='". $deps[$i]['dep_id'] . "'";
//				}
//				$user_id .= ") ";


			}
			else
                $user_id = "org1.user_id='".$request->getParam('user_id')."'";
            if(JAVADB) $user_id ="org1.id_user='".$request->getParam('user_id')."'";

		} else {
			$user_id='1=1';
		}

		//HACK если стажер -- то всегда видит только свое
        if (getUserLevel()==LEVEL_INTERN) {
            $user_id = "org1.user_id=".getUserId()."";
        }


		if($request->getParam('status_id')){
			$status_id = "org1.status_id='".$request->getParam('status_id')."'";
		} else {
			$status_id='1=1';
		}


        if($request->getParam('dep_ids')){
            $dep_id = "AND dep_org.org_id=org1.org_id and user.user_id = org1.user_id AND dep_org.dep_id='".$request->getParam('dep_ids')."'";
            $dep_org = 'dep_org,';
        }
        else {
            $dep_id=''; $dep_org='';
        }


        $per_page=($request->getParam('per_page')) ? $request->getParam('per_page') : 30;

		$page = $request->getParam('page') ? $request->getParam('page') : 1;

        // if($page==1) $offset = 0; else 	$offset = $page * $per_page;
       	$offset = ($page-1) * $per_page;

//        $q="SELECT SQL_CALC_FOUND_ROWS
//		org1.id org_id, org1.name name_short, org1.email,
//		org1.address address_fact, org1.city,
//		user.name, org1.info,
//		 org1.action_last, org1.action_next, org1.action_count,
//		FROM  user tab_user, tab_client org1
//		WHERE org1.hide=0 AND user.user_id=org1.user_id
//		AND	($search_str_q) AND ($search_city) AND ($user_id)
//		ORDER BY $orderby_field $orderby_dir
//		LIMIT $offset, $per_page";


        $q="SELECT SQL_CALC_FOUND_ROWS
		org1.org_id, org1.status_id, org1.name_short, org1.phone1, org1.phone2, org1.email,
		org1.address_fact, org1.city,
		user.login, user.name,user.surname, org_status.org_status, org1._other, org1.info,
		 org1.action_last, org1.action_next, org1.action_count,
		 (select group_concat(dep_id) from  dep_org  where dep_org.org_id = org1.org_id group by org_id ) deps_id
		FROM   $dep_org user, org org1

		LEFT JOIN org_status ON org1.status_id=org_status.status_id
		WHERE org1.hide=0 AND user.user_id=org1.user_id
		AND	($search_str_q) AND ($search_city) AND ($user_id) AND ($status_id) $dep_id
		ORDER BY $orderby_field $orderby_dir
		LIMIT $offset, $per_page";

        if(JAVADB) {
            $q = "SELECT SQL_CALC_FOUND_ROWS
		org1.id org_id, org1.name name_short, org1.email,
		org1.address address_fact, org1.city,
		user.name,user.surname, org1.info,
		 org1.action_last, org1.action_next, org1.action_count
		FROM   tab_user user, tab_client org1
		WHERE user.id=org1.id_user
		AND	($search_str_q) AND ($search_city) AND ($user_id)
		ORDER BY $orderby_field $orderby_dir
		LIMIT $offset, $per_page";
        }





        $rows = $this->db->rawQuery($q);
		$total_rows = $this->db->rawQuery("SELECT FOUND_ROWS()");

		if ($this->db->getLastErrno() === 0) {
			$arr['success'] = true;
			$arr['firms'] =$rows;
			$arr['filters']['cities'] =$cities;
			$arr['filters']['users'] =$users;
			$arr['filters']['statuses'] =$statuses;

			if ($total_rows >= $per_page) {
				$arr['meta']['pre_perpage'] =$page+1;
			}
			else{
				$arr['meta']['pre_perpage'] =0;
			}

			$arr['total']= $total_rows[0]['FOUND_ROWS()']; //$this->db->totalCount;


		} else {
			$arr['success'] = false;
			$arr['errinfo'] = $this->db->getLastError();
		}


	// $arr['search_str']=$search_str;
		$arr['db_debug'] =  $this->db->trace;

		return $response->withJson($arr);

	}




	public function get ($request, $response, $args) {
		// return $response->write("Hello " . $args['firm_id']);

		$db = $this->db;
		$sort = $request->getParam('sort'); // не совсем сортировка, точнее было назвать фильтр -- все, свои, избранное,
		$page = ($request->getParam('page')) ? $request->getParam('page') : 1 ;

		$perpage=($request->getParam('count')) ? $request->getParam('count') : 30;
		$offset =$perpage*($page-1);

		$current_user_id = getUserId();

// пока использую только для sort=all
		$orderby_field = ($request->getParam('orderby_field')) ? $request->getParam('orderby_field') : 'name_short' ;
		$orderby_dir = ($request->getParam('orderby_dir')) ? $request->getParam('orderby_dir') : 'ASC' ;


		// доступ на просмотр (и выше) всех фирм в направлении. коряво, потом переделать
		$where_level =" AND org1.org_id IN (select distinct dep_org.org_id
		from dep_user, dep_org
		where
		dep_user.dep_id=dep_org.dep_id AND dep_user.level>1 AND
		user_id=$current_user_id)";

		$only_mine = "AND org1.user_id=$current_user_id";

		$org_fav_id ="(select org_fav_id from org_fav where org_fav.org_id=org1.org_id) org_fav_id,";
		// для каждой организации, смотрим есть ли она уже в избранных. эта строка будет вставлена после SELECT поэтому в конце запятая.


		if ($sort == 'next') {
			$q = "SELECT org1.org_id, org1.name_short, action.txt, DATE_FORMAT(action.datetime1,'%d-%m-%y %H:%i') datetime1, DATE_FORMAT(action.future,'%d-%m-%y') future, org1.status_id,action.contact_person,
			user.login, user.name, org_fav.user_id AS is_in_fav,
			(SELECT GROUP_CONCAT(dep SEPARATOR ', ') FROM dep, dep_org WHERE dep.dep_id=dep_org.dep_id AND dep_org.org_id = org1.org_id) AS deps
			FROM  user, action,
			(SELECT org_id, MAX(future) AS max_future
			FROM action
			WHERE hide=0
			GROUP BY org_id) as t1, org org1
			LEFT JOIN org_fav ON org1.org_id=org_fav.org_id AND org_fav.user_id='$current_user_id'
			WHERE org1.hide=0  AND org1.org_id = t1.org_id AND action.org_id=org1.org_id AND t1.max_future=action.future AND org1.hide=0 AND user.user_id=org1.user_id $only_mine
			order by action.future ASC
			LIMIT $offset, $perpage";

			$count = $db->rawQuery("SELECT count(org_id) total FROM org org1 WHERE org1.hide=0 $only_mine");
			$count = $count[0]['total'];

		}
		elseif($sort == 'today'){
            $sql_mode = $db->rawQuery("SET SESSION sql_mode = ''");


            $q="
			select * from

 (SELECT  org1.org_id, name_short, txt, future, datetime1,
org1.phone1,org1.person1,org1.phone2,org1.person2,org1.phone3,org1.person3
 FROM action, org org1, user
 WHERE
action.org_id=org1.org_id AND 
user.user_id=action.user_id AND
future > DATE_SUB(NOW(), INTERVAL 7 DAY)  AND
future < DATE_ADD(NOW(), INTERVAL 1 DAY)
-- $only_mine контакт и напоминание может быть и на чужом клиенте
AND action.user_id = $current_user_id
order by future desc
limit 100)

 as A 
GROUP BY org_id
ORDER BY future DESC;";

//
//$count = $db->rawQuery("SELECT count(org_id) total FROM org org1 WHERE org1.hide=0 $where_level");
//$count = $count[0]['total'];
            $count = 111;

        }
		elseif ($sort == 'contact') {
			// TODO на уровне проектирования непонятка -- выводить ли тут только мои, или все - т.е. и чужие, в которых  я событие оставил. пока только свои фирмы
			$q = "SELECT org1.org_id, org1.name_short,action.txt, DATE_FORMAT(action.datetime1,'%d-%m-%y %H:%i') datetime1, DATE_FORMAT(action.future,'%d-%m-%y') future, org1.status_id,action.contact_person,
			user.login, user.name, org_fav.user_id AS is_in_fav,
			(SELECT GROUP_CONCAT(dep SEPARATOR ', ') FROM dep, dep_org WHERE dep.dep_id=dep_org.dep_id AND dep_org.org_id = org1.org_id) AS deps
			FROM user, action,
			(SELECT org_id, MAX(action_id) AS max_action_id
			FROM action
			WHERE hide=0
			GROUP BY org_id) as t1, org org1
			LEFT JOIN org_fav ON org1.org_id=org_fav.org_id AND org_fav.user_id='$current_user_id'
			WHERE  org1.hide=0 AND org1.org_id = t1.org_id AND action.org_id=org1.org_id AND t1.max_action_id=action.action_id AND  user.user_id=org1.user_id $only_mine
			order by action.datetime1 DESC
			LIMIT $offset, $perpage";

			$count = $db->rawQuery("SELECT count(org_id) total FROM org org1 WHERE org1.hide=0 $only_mine");
			$count = $count[0]['total'];

		}
		elseif ($sort == 'mine') {

			$q="SELECT  DATE_FORMAT(a.future,'%d-%m-%y') future,
			DATE_FORMAT(a.datetime1,'%d-%m-%y %H:%i') datetime1,org1.status_id,
			a.txt,org1.org_id, org1.name_short,lastaction.max_action_id,a.contact_person,
			user.login, user.name, org_fav.user_id AS is_in_fav,
			(SELECT GROUP_CONCAT(dep SEPARATOR ', ') FROM dep, dep_org WHERE dep.dep_id=dep_org.dep_id AND dep_org.org_id = org1.org_id) AS deps
			FROM    user, org org1
			LEFT JOIN
			(SELECT org_id, MAX(action_id)  max_action_id
			FROM action
			WHERE hide=0
			GROUP BY org_id) lastaction
			ON org1.org_id  =lastaction.org_id
			LEFT JOIN action a ON lastaction.max_action_id = a.action_id
			LEFT JOIN org_fav ON org1.org_id=org_fav.org_id AND org_fav.user_id='$current_user_id'
			WHERE org1.hide=0  AND user.user_id=org1.user_id $only_mine
			ORDER BY org1.org_id DESC
			LIMIT $offset, $perpage";

			$count = $db->rawQuery("SELECT count(org_id) total FROM org org1 WHERE org1.hide=0 $only_mine");
			$count = $count[0]['total'];


		}
		elseif ($sort == 'fav') {
//			$only_fav_org = " AND org1.org_id=org_fav.org_id AND org_fav.user_id=$current_user_id";
//			$q="SELECT
//			DATE_FORMAT(a.future,'%d-%m-%y') future,
//			DATE_FORMAT(a.datetime1,'%d-%m-%y %H:%i') datetime1,org1.status_id,
//			a.txt,org1.org_id, org1.name_short,lastaction.max_action_id,a.contact_person,
//			user.login, user.name, org_fav.user_id AS is_in_fav,
//			(SELECT GROUP_CONCAT(dep SEPARATOR ', ')
//			FROM dep, dep_org
//			WHERE dep.dep_id=dep_org.dep_id AND dep_org.org_id = org1.org_id) AS deps
//			FROM    user,org_fav, org org1
//			LEFT JOIN
//			(SELECT org_id, MAX(action_id)  max_action_id
//			FROM action
//			WHERE hide=0
//			GROUP BY org_id) lastaction
//			ON org1.org_id  =lastaction.org_id
//			LEFT JOIN action a ON lastaction.max_action_id = a.action_id
//			WHERE org1.hide=0  AND user.user_id=org1.user_id $only_fav_org
//			ORDER BY org1.org_id DESC
//			LIMIT $offset, $perpage";
//
            $q="SELECT SQL_CALC_FOUND_ROWS
		org1.org_id, org1.status_id, org1.name_short, org1.phone1, org1.phone2, org1.email,
		org1.address_fact, org1.city,1 is_in_fav, 
		user.login, user.name, org_status.org_status, org1._other, org1.info,
		 org1.action_last, org1.action_count
		FROM  user, org_fav, org org1

		LEFT JOIN org_status ON org1.status_id=org_status.status_id
		WHERE org1.hide=0 AND user.user_id=org1.user_id and org_fav.user_id = $current_user_id and org_fav.org_id=org1.org_id 
		ORDER BY org1.name_short";

    		$count = 999;


		}


		elseif ($sort == 'all') {

			$q="SELECT
			DATE_FORMAT(a.future,'%d-%m-%y') future,
			DATE_FORMAT(a.datetime1,'%d-%m-%y') datetime1,org1.status_id,
			a.txt,org1.org_id, org1.name_short, org1.phone1, org1.phone2, org1.email,
			lastaction.max_action_id,a.contact_person,org1.address_fact, org1.city,
			user.login, user.name, t.label,

			(SELECT GROUP_CONCAT(dep SEPARATOR ', ') FROM dep, dep_org WHERE dep.dep_id=dep_org.dep_id AND dep_org.org_id = org1.org_id) AS deps, org_fav.user_id AS is_in_fav

			FROM    user, org org1
			LEFT JOIN
			(SELECT org_id, MAX(action_id)  max_action_id
			FROM action
			WHERE hide=0
			GROUP BY org_id ) lastaction
			ON org1.org_id  =lastaction.org_id
			LEFT JOIN action a ON lastaction.max_action_id = a.action_id
			LEFT JOIN task t ON t.task_id=a.task_id
			LEFT JOIN org_fav ON org1.org_id=org_fav.org_id AND org_fav.user_id='$current_user_id'
			WHERE org1.hide=0 AND user.user_id=org1.user_id $where_level
			ORDER BY $orderby_field $orderby_dir
			LIMIT $offset, $perpage";


			$count = $db->rawQuery("SELECT count(org_id) total FROM org org1 WHERE org1.hide=0 $where_level");
			$count = $count[0]['total'];
		}



		// $firms = $db->map('org_id')->ArrayBuilder()->rawQuery($q);
		$firms = $db->rawQuery($q);

		if($count > $perpage){

			if($page>0) $pre_perpage=$page+1; else $pre_perpage=1;
			if($page==0) $next_perpage=0; else $next_perpage=$page-1;

		} else {
			$pre_perpage='';
			$next_perpage='';
		}

		foreach ($firms as $key => $firm) {

			// if($firm['status_id']==STATUS_VIPCLIENT){
			// 	$firms[$key]['ui_is_vip'] = true;
			// }
			//
		// elseif($firm['status_id']==STATUS_PROJECT){
		//     $firms[$key]['ui_is_project'] = true;
		// }
		}

// print_r($firms);
// die('==');

		$out= array(
			'list_title' => 'Список организаций',
			'list_info' => "Показано $perpage из ".$count,
			'list_error' => '',
			'perpage' =>$perpage,
			'pre_perpage'=>$pre_perpage,
			'next_perpage'=>$next_perpage,
			'sortby'=>$sort,
			'firms' => $firms,
						// 'ui_show_all' => $ui_show_all,
			);

		$out['db_debug'] = $db->trace;

		return $response->withJson($out);
	}

	public function get_item ($request, $response, $args) {
		global $ORG_STATUS;
		$db = $this->db;

		//  echo $db->getLastQuery();

			// получаем информацию о фирме
		$q1 = "SELECT  o.*, user.name, user.surname, org_fav.user_id AS is_in_fav,d.aolevel,d.parentguid
		FROM user, org o
		LEFT JOIN org_fav ON o.org_id=org_fav.org_id
		LEFT JOIN d_fias_addrobj d ON d.aoguid=o.fias_aoguid
		WHERE user.user_id= o.user_id AND o.org_id = ".$args['id'];

        if(JAVADB) {
            $q1 = "SELECT  tab_client.id org_id, tab_client.status status_id, tab_client.name name_short,
 postalcode,city, street, address, tab_client.phone phone1, persons person1, 
 tab_client.email, site, info, tab_user.name, tab_user.surname
		FROM tab_user, tab_client
		WHERE tab_user.id= tab_client.id_user AND tab_client.id = ".$args['id'];
        }


		$firm_info = $db->rawQuery($q1);
		$firm_info = $firm_info[0];



        if(JAVADB) {
            // получаем активности по данной фирме
                $q_actions="SELECT tab_action.id action_id, actiondt datetime1, theme txt, adopted contact_person,
 id_user user_id, futuredt future, tab_user.name,tab_user.surname  
FROM tab_action,tab_user WHERE tab_user.id = tab_action.id_user AND id_client=$args[id] 
ORDER BY datetime1 DESC";
            $firm_actions = $db->rawQuery($q_actions);
        }
        else {
            // получаем активности по данной фирме
            $db->join('action a', 'o.org_id=a.org_id', 'INNER');
            $db->join('user u', 'u.user_id=a.user_id', 'INNER');
            $db->join('task t', 't.task_id=a.task_id', 'LEFT');
            $db->where('o.org_id', $args['id']);
            $db->where('a.hide', 0);
            $db->orderBy('a.datetime1', 'DESC');
            $firm_actions = $db->get('org o', 30, ['action_id','txt','a.contact_person', 'datetime1',
                'future', 'action_type_id','u.name','u.surname','t.label']);
        }


		 // получаем задачи
//		$db->where('hide', 0);
//		$db->orderBy('task_id', 'DESC');
//		$tasks = $db->get('task', 30);


		 //подразделения (для редактирования)
		// $db->join('dep_org b', 'a.dep_id=b.dep_id', 'LEFT');
		// $db->joinWhere("dep_org b", "b.org_id", $args['id']);
		// $db->orderBy('dep_id', 'ASC');
		// $db->where('hide', 0);
		// $deps = $db->get('dep a',999,['a.dep','a.dep_id','b.dep_id selected']);

        if(JAVADB) {
            $q= "SELECT phone, email, person, comment 
                FROM tab_phone WHERE id_client='$args[id]'";
            $contacts = $db->rawQuery($q);
//            $contacts = ['phone'=>'8888888'];
        }
        else{
            $db->where('org_id', $args['id']);
            $contacts = $db->get('org_contact');
        }

		$db->where('org_id', $args['id']);
		$db->orderBy('dep_org.dep_id', 'ASC');
		$db->join('dep', 'dep_org.dep_id=dep.dep_id', 'INNER');
		$deps = $db->get('dep_org');
		$dep_ids = [];
		foreach ($deps as $key=>$dep_id) {
//			$dep_ids[$key] = (string)$dep_id['dep_id'];
            $dep_ids[$key] = $dep_id['dep_id'];
		}



		//  $user_level=getUserLevel();
		//  $user_id = getUserId();
		// если доступ выше юзера (т.е. менеджер или админ, а не юзер или стажер) то даем возможность редактировать
		$this->db->where('user_id', getUserId());
		$dep_user = $this->db->get('dep_user');
		for ($i=0; $i < count($dep_user); $i++) {
			$dep_user[$i] = $dep_user[$i]['dep_id'];
		}
		$dep_comp = array_uintersect($dep_ids, $dep_user, "strcasecmp");
		if(getUserLevel() > LEVEL_USER OR getUserId()== @$firm_info['user_id'] OR count($dep_comp)) {
			$arr['editable'] = true;
		} else{
			$arr['editable'] = false;
		}

//		$firm_info['city_guid'] = $firm_info['aolevel'] == 7 ? $firm_info['parentguid'] : $firm_info['fias_aoguid'];
		$arr['firm_info'] = $firm_info;
		$arr['actions'] = $firm_actions;
		$arr['firm_info']['dep_ids'] = $dep_ids;


		$arr['deps'] = $deps;
		$arr['contacts'] = $contacts;
//		$arr['tasks'] = $tasks;

		@$arr['status'] = $ORG_STATUS[$firm_info['status_id']];
		$arr['db_debug'] = $db->trace;

		return $response->withJson($arr)->withHeader('Content-Type','application/json');
	}

	public function add ($request, $response, $args) {
		$db = $this->db;

        if(JAVADB) {
            $params = $request->getParams();
//            $d = $request->getParams();
//            $d['id_user'] =getUserId();






//            $q = "INSERT INTO tab_client SET name='$d[name_short]',
//email=$d[email], site=$d[site], phone=$d[phone1], persons=$d[person1], address=$d[address_fact],
//city=$d[city], status=$d[status_id], info=$d[info], postalcode=$d[postalcode], street=$d[street],
//inn=$d[inn], kpp=$d[kpp], ogrn=$d[ogrn], guid_client=uuid(), tmst=NOW(),
//un_user=(SELECT un_user FROM tab_user WHERE id=$d[id_user])";
//
//            $res = $this->db->rawQuery($q);
//            $insert_id = $this->db->rawQuery('SELECT LAST_INSERT_ID() lastid;');
//            $insert_org_id = $insert_id[0]['lastid'];


            if (array_key_exists("name_short",$params)) $data['name'] = $params['name_short'];

            if (array_key_exists("email",$params)) $data['email'] = $params['email'];
            if (array_key_exists("site",$params)) $data['site'] = $params['site'];
            if (array_key_exists("phone1",$params)) $data['phone'] = $params['phone1'];
            if (array_key_exists("person1",$params)) $data['persons'] = $params['person1'];
            if (array_key_exists("address_fact",$params)) $data['address'] = $params['address_fact'];
            if (array_key_exists("city",$params)) $data['city'] = $params['city'];

            if (array_key_exists("status_id",$params)) $data['status'] = $params['status_id'];

            if (array_key_exists("info",$params)) $data['info'] = $params['info'];
            if (array_key_exists("postalcode",$params)) $data['postalcode'] = $params['postalcode'];
          if (array_key_exists("street",$params)) $data['street'] = $params['street'];
            if (array_key_exists("inn",$params)) $data['inn'] = $params['inn'];
            if (array_key_exists("kpp",$params)) $data['kpp'] = $params['kpp'];
            if (array_key_exists("ogrn",$params)) $data['ogrn'] = $params['ogrn'];
            $data['guid_client'] = uuid();
            $data['tmst'] = date('Y-m-d');
            $data['un_user'] ="WEB_USER";
            $data['id_user'] =getUserId();

            $insert_org_id  = $db->insert('tab_client', $data);

        }
        else{
            $data = array(
                'name_short' => $request->getParam('name_short'),
                'user_id' => getUserId()
            );

            if ($request->getParam('email')) $data['email'] = $request->getParam('email');
            if ($request->getParam('site')) $data['site'] = $request->getParam('site');
            if ($request->getParam('phone1')) $data['phone1'] = $request->getParam('phone1');
            if ($request->getParam('person1')) $data['person1'] = $request->getParam('person1');
            if ($request->getParam('person1title')) $data['person1title'] = $request->getParam('person1title');
            if ($request->getParam('phone2')) $data['phone2'] = $request->getParam('phone2');
            if ($request->getParam('person2')) $data['person2'] = $request->getParam('person2');
            if ($request->getParam('person2title')) $data['person2title'] = $request->getParam('person2title');
            if ($request->getParam('phone3')) $data['phone3'] = $request->getParam('phone3');
            if ($request->getParam('person3')) $data['person3'] = $request->getParam('person3');
            if ($request->getParam('person3title')) $data['person3title'] = $request->getParam('person3title');
            if ($request->getParam('address_fact')) $data['address_fact'] = $request->getParam('address_fact');
            if ($request->getParam('city')) $data['city'] = $request->getParam('city');
            if ($request->getParam('street')) $data['street'] = $request->getParam('street');
            if ($request->getParam('inn')) $data['inn'] = $request->getParam('inn');
            if ($request->getParam('kpp')) $data['kpp'] = $request->getParam('kpp');
            if ($request->getParam('ogrn')) $data['ogrn'] = $request->getParam('ogrn');

            if ($request->getParam('info')) $data['info'] = $request->getParam('info');

            if ($request->getParam('status_id')) $data['status_id'] = $request->getParam('status_id');


            $insert_org_id = $db->insert('org', $data);
        }



		$deps = $request->getParam('dep_ids');

		if ($deps) {
			foreach ($deps as  $dep_id) {
				$insert = array(
					'org_id' => $insert_org_id,
					'dep_id' => $dep_id,
					);
				$deps_insert_id = $db->insert('dep_org', $insert);
			}
		}

		if ($db->getLastErrno() === 0) {
			$arr['success'] = true;
			$arr['insert_id'] = $insert_org_id;
		} else {
			$arr['success'] = false;
			$arr['errinfo'] = $db->getLastError();
		}

		return $response->withJson($arr);
	}

	public function edit_item ($request, $response, $args) {
		$db = $this->db;
		// return $response->write("Hello " . $args['firm_id']);
		$params = $request->getParams();


        if(JAVADB) {
            $data = array('name' => $params['name_short']);

            if (array_key_exists("email",$params)) $data['email'] = $params['email'];
            if (array_key_exists("site",$params)) $data['site'] = $params['site'];
            if (array_key_exists("phone1",$params)) $data['phone'] = $params['phone1'];
            if (array_key_exists("person1",$params)) $data['persons'] = $params['person1'];
            if (array_key_exists("address_fact",$params)) $data['address'] = $params['address_fact'];
            if (array_key_exists("city",$params)) $data['city'] = $params['city'];

            if (array_key_exists("status_id",$params)) $data['status'] = $params['status_id'];

            if (array_key_exists("info",$params)) $data['info'] = $params['info'];
            if (array_key_exists("postalcode",$params)) $data['postalcode'] = $params['postalcode'];
//            if (array_key_exists("aoguid",$params)) $data['fias_aoguid'] = $params['aoguid'];
            if (array_key_exists("street",$params)) $data['street'] = $params['street'];
            if (array_key_exists("user_id",$params)) $data['id_user'] = $params['user_id'];
            if (array_key_exists("inn",$params)) $data['inn'] = $params['inn'];
            if (array_key_exists("kpp",$params)) $data['kpp'] = $params['kpp'];
            if (array_key_exists("ogrn",$params)) $data['ogrn'] = $params['ogrn'];

            $db->where('id', $args['id']);
            $update_result  = $db->update('tab_client', $data, 1);

        }
        else {
            if (array_key_exists("delete",$params)) if($params['delete']=='on') $hide = 1; else  $hide = 0;else  $hide = 0;

            $data = array(
                'name_short' => $params['name_short'],
                'hide' => $hide
            );

            if (array_key_exists("email",$params)) $data['email'] = $params['email'];
            if (array_key_exists("site",$params)) $data['site'] = $params['site'];
            if (array_key_exists("phone1",$params)) $data['phone1'] = $params['phone1'];
            if (array_key_exists("person1",$params)) $data['person1'] = $params['person1'];
            if (array_key_exists("person1title",$params)) $data['person1title'] = $params['person1title'];
            if (array_key_exists("address_fact",$params)) $data['address_fact'] = $params['address_fact'];
            if (array_key_exists("city",$params)) $data['city'] = $params['city'];
            if (array_key_exists("status_id",$params)) $data['status_id'] = $params['status_id'];
            if (array_key_exists("info",$params)) $data['info'] = $params['info'];
            if (array_key_exists("postalcode",$params)) $data['postalcode'] = $params['postalcode'];
            if (array_key_exists("aoguid",$params)) $data['fias_aoguid'] = $params['aoguid'];
            if (array_key_exists("street",$params)) $data['street'] = $params['street'];
            if (array_key_exists("user_id",$params)) $data['user_id'] = $params['user_id'];

            if (array_key_exists("inn",$params)) $data['inn'] = $params['inn'];
            if (array_key_exists("kpp",$params)) $data['kpp'] = $params['kpp'];
            if (array_key_exists("ogrn",$params)) $data['ogrn'] = $params['ogrn'];

            if (array_key_exists("hide",$params) AND $params['hide']=='true') $data['hide'] = 1;

            $db->where('org_id', $args['id']);
            $update_result  = $db->update('org', $data, 1);
        }




		$db->where('org_id', $args['id']);
		$delete_result = $db->delete('dep_org');

		if (array_key_exists("dep_ids",$params)) $deps = $params['dep_ids']; else $deps = [];
		if($deps){

			foreach ($deps as $dep_id) {
				$insert = array(
					'org_id' => $args['id'],
					'dep_id' => $dep_id
					);
				$level_insert_id = $db->insert('dep_org', $insert);
			}

		}
		if ($update_result) {
			$arr['success'] = true;
		} else {
			$arr['success'] = false;
			$arr['errinfo'] = $db->getLastError();
		}

		return $response->withJson($arr);
	}

	public function search ($request, $response, $args) {
		$db = $this->db;

		// $db->where('org_id', $args['firm_id']);
		// $firm_info = $db->get('org', 1);

		// $db->join('action a', 'o.org_id=a.org_id', 'INNER');
		$query = $request->getParam('search_str');

		$db->where('name_short', "%${query}%", 'LIKE');
		$db->orderBy('name_short', 'ASC');
		$firms = $db->get('org', 10, ['name_short', 'org_id']);

		$arrout = $this->result($firms,'firms');

		return $response->withJson($arrout);
	}

	public function fav_item ($request, $response,$args) {
		$id = $args['id'];
		$user_id = getUserId();

		$data = ['org_id' => $id, 'user_id'=>$user_id];

		$last_id = $this->db->insert ('org_fav', $data);

		$out = $this->result($last_id,'message');

		return $response->withJson($out);
	}

	public function unfav_item ($request, $response,$args) {
		$id = $args['id'];
		$user_id = getUserId();

		$this->db->where('org_id', $id);
		$this->db->where('user_id', $user_id);
		$result = $this->db->delete('org_fav');

		$out = $this->result($result,'message');

		return $response->withJson($out);
	}

	public function addcontact_item($request, $response,$args){



		$data['phone'] = $request->getParam('phone')? $request->getParam('phone') : '';
		$data['email'] = $request->getParam('email')? $request->getParam('email') : '';
		$data['person'] = $request->getParam('person')? $request->getParam('person') : '';
		$data['comment'] = $request->getParam('comment')? $request->getParam('comment') : '';

        if(JAVADB) {
            $data['id_client'] = $args['id'];

            // TODO java --  вставлять гуид клиента, чтобы в джава версии отображалось
            $data['guid_client'] = 'xxxx-xxxx';// $this->db->rawQueryValue('select guid_client from tab_client where id='.$args['id']);

            $id = $this->db->insert ('tab_phone', $data);

        }
        else {
            $data['org_id'] = $args['id'];
            $id = $this->db->insert ('org_contact', $data);

        }

		$this->db->where('org_contact_id', $id);
		$contact = $this->db->get('org_contact');

		@$result = $this->result($contact[0]);
		$result = $this->result($id,'insert_id',$result);

		return $response->withJson($result);
	}

	public function getcontacts_item($request, $response,$args){
		$this->db->where('org_id', $args['id']);
		$contacts = $this->db->get('org_contact');

		$this->db->where('org_id', $args['id']);
		$contact = $this->db->get('org',1, ['person1', 'phone1','person1title','email']);
		$contacts[] = array('person' => $contact[0]['person1'],'phone' => $contact[0]['phone1'],'comment' => $contact[0]['person1title'],'email' => $contact[0]['email'], );
		$result = $this->result($contacts);

		return $response->withJson($result);
	}

	public function editcontact_item($request, $response,$args){
		if (count($args['params']) == 3)
			$this->db->where('org_contact_id',$args['params'][2]);
		$data['org_id'] = $args['id'];
		$data['phone'] = $request->getParam('phone')? $request->getParam('phone') : '';
		$data['email'] = $request->getParam('email')? $request->getParam('email') : '';
		$data['person'] = $request->getParam('person')? $request->getParam('person') : '';
		$data['comment'] = $request->getParam('comment')? $request->getParam('comment') : '';
		if($this->db->update('org_contact',$data)){
			$this->db->where('org_contact_id', $args['params'][2]);
			$contact = $this->db->get('org_contact');

			$result = $this->result($contact[0]);
			return $response->withJson($result);
		}else return $response->withJson(['success'=>false]);
		return $response->withJson(['success'=>false]);
	}

	public function deletecontact_item($request, $response,$args){
		if (count($args['params']) == 3)
			$this->db->where('org_contact_id',$args['params'][2]);
		$this->db->where('org_id',$args['id']);
		if($this->db->delete('org_contact')) return $response->withJson(['success'=>true]);
		return $response->withJson(['success'=>false]);
		return $response->withJson(['success'=>false]);
		// $data['org_id'] = $args['id'];
		// $data['phone'] = $request->getParam('phone')? $request->getParam('phone') : '';
		// $data['email'] = $request->getParam('email')? $request->getParam('email') : '';
		// $data['person'] = $request->getParam('person')? $request->getParam('person') : '';
		// $id = $this->db->insert ('org_contact', $data);

		// $this->db->where('org_contact_id', $id);
		// $contact = $this->db->get('org_contact');

		// $result = $this->result($contact);
		// $result = $this->result($id,'insert_id',$result);

		// return $response->withJson($result);
	}

	public function getstatuses($request, $response,$args){

		$statuses = $this->db->get('org_status');
		return $response->withJson($this->result($statuses));
	}
}

<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Project_comment extends AbstractController{
	
	public function get ($request, $response, $args) {

		$this->db->orderBy('timestamp', 'ASC');
		$this->db->where('deleted', 0);
		if ($request->getParam('project_id')) $this->db->where('project_id',$request->getParam('project_id'));
		$this->db->join("user u", "project_comment.user_id=u.user_id", "LEFT");
		$comments = $this->db->map('id')->ArrayBuilder()->get('project_comment');

		return $response->withJson($this->result($comments ? $comments : new ArrayObject()));
	}
	
	public function get_item ($request, $response, $args) {
		
		$this->db->where('id', $args['id']);
		$comment = $this->db->get('project_comment');
		return $response->withJson($this->result($comment));
	}

	public function add ($request, $response, $args) {
		
		$project_id = $request->getParam('project_id', $default = -1);
		$title = $request->getParam('title', $default = "");
		$content = $request->getParam('content', $default = "");
		$data = array('user_id' => getUserId(),'project_id' => $project_id, 'content' => $content, 'title' => $title);
		$id = $this->db->insert('project_comment', $data);
		

		$this->db->where('id', $id);
		$this->db->join("user u", "project_comment.user_id=u.user_id", "LEFT");
		$comment = $this->db->get('project_comment');
		$result = $this->result($comment[0]);
		$result = $this->result($id,'insert_id',$result);
		return $response->withJson($result);
	}

	public function delete_item($request, $response, $args){
		
		$this->db->where('id', $args['id']);
		$result = $this->db->update('project_comment',Array ('deleted' => 1));

		return $response->withJson($this->result($result));
	}
}
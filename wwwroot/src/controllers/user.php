<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class User extends AbstractController{

	public function get ($request, $response, $args) {
		$db = $this->db;

		$q="select user_id, name, surname,user_level, login,email
		from user o
		where hide=0
		order by user_id DESC";

//        $q="select user_id, name, surname,user_level, login,email,
//
//		(select group_concat(DISTINCT dep SEPARATOR ', ')
//		from dep,dep_user where dep.dep_id=dep_user.dep_id AND dep_user.user_id=o.user_id) deps
//
//		from user o
//		where hide=0
//		order by user_id DESC";


        $users = $db->map('user_id')->ArrayBuilder()->rawQuery($q);

		$level_name[1]='Стажер';
		$level_name[2]='Пользователь';
		$level_name[3]='Менеджер';
		$level_name[4]='Администратор';

		foreach ($users as $key => $user1) {
			@$users[$key]['user_level_name'] = $level_name[$user1['user_level']];
		}

		if ($db->getLastErrno() === 0) {
			$arr['success'] = true;
			$arr['users'] = $users;
		} else {
			$arr['success'] = false;
			$arr['errinfo'] = $db->getLastError();
		}


		return $response->withJson($arr);
	}

	public function get_item ($request, $response, $args) {
		$db = $this->db;

		$id = $args['id'] ? $args['id'] : getUserId();
		$db->where('user_id', $id);
		$user = $db->get('user');
		$user = $user[0];

		// получаем список подразделений
//		$db->where('user_id',$id);
//		$deps = $db->get('dep_user');

		// отключил фичу многих подразделений.  теперь только одно
//		foreach ($deps as $key => $dep) {
//			$deps[$key] = (string)$dep['dep_id'];
//		}
//		$user['deps'] = $deps;
//  		@$user['dep_id'] = $deps[0]['dep_id'];

		$result = $this->result($user);

		return $response->withJson($result);
	}

	public function add ($request, $response, $args) {
		$db = $this->db;
		$params = $request->getParams();
		@$data = array(
			'name' => $params['name'],
			'middlename' => $params['middlename'],
			'surname' => $params['surname'],
			'email' => $params['email'],
			'phone1' => $params['phone1'],
			'phone2' => $params['phone2'],
			'login' => $params['login'],
			'password' => $params['password'],
			'user_level' => $params['user_level'],
			'hour_rate_margin' => $params['hour_rate_margin'],
			'hour_rate' => $params['hour_rate']
			);

		$insert_id = $db->insert('user', $data);
		$bad_insert = $db->getLastErrno();

		if (!$bad_insert) {
			$arr['success'] = true;
			$arr['insert_id'] = $insert_id;
		} else {
			$arr['success'] = false;
			$arr['errinfo'] = $db->getLastError();
		}

		$levels = $params['user_level'];

			// если вообще установлены права
		if (!$bad_insert) {

//			$deps = $params['deps'];
			// отключил многие подразделения, оставил пока одно

//				$insert = array(
//					'user_id' => $insert_id,
//					'dep_id' =>$params['dep_id'],
//					'level' => 0,
//				);
//		$db->insert('dep_user', $insert);

			// foreach ($deps as $key => $val) {
			// 	$insert = array(
			// 		'user_id' => $args['id'],
			// 		'dep_id' => $val,
			// 		'level' => 0,
			// 	);
			// 	$db->insert('dep_user', $insert);
			// }

// создаем канбан-списки для нового юзера
          $kanban =  " INSERT INTO project_todo
            (project_id,parent_id,project_status_id, title, user_ids,children_order_json)
 VALUES (0,0,1,'Все задачи',$insert_id,'[]'),(0,0,2,'Скоро',$insert_id,'[]'),
         (0,0,3,'В работе',$insert_id,'[]'),(0,0,4,'Сделано!',$insert_id,'[]'),
  (0,0,0,'Корзина',$insert_id,'[]');";

            $res = $db->rawQuery($kanban);

		}

		return $response->withJson($arr);
	}

	public function edit_item ($request, $response, $args) {
		$db = $this->db;
		$params = $request->getParams();

		$data = array(
			'name' => $params['name'],
			'middlename' => $params['middlename'],
			'surname' => $params['surname'],
			'email' => $params['email'],
			'phone1' => $params['phone1'],
			'phone2' => $params['phone2'],
			'login' => $params['login'],
			'password' => $params['password'],
			'user_level' => $params['user_level'],
			'hide' =>$params['hide'],
			'hour_rate_margin' => $params['hour_rate_margin'],
			'hour_rate' => $params['hour_rate']

			);
		$db->where('user_id', $args['id']);
		$bad_insert = $db->getLastErrno();
		$update_id = $db->update('user', $data, 1);

		if (!$bad_insert) {
			$arr['success'] = true;
		} else {
			$arr['success'] = false;
			$arr['errinfo'] = $bad_insert;
		}

		// обновляем права. сначала удаляем все, потом делаем insert. так как-то проще, чем update
		if (!$bad_insert) {
//			$db->where('user_id', $args['id']);
//			$delete_result = $db->delete('dep_user');
//
//			$deps = $params['dep_id'];
//
//			$insert = array(
//				'user_id' => $args['id'],
//				'dep_id' => $deps,
//				'level' => 0,
//			);
//	$db->insert('dep_user', $insert);

			// foreach ($deps as $key => $val) {
			// 	$insert = array(
			// 		'user_id' => $args['id'],
			// 		'dep_id' => $val,
			// 		'level' => 0,
			// 	);
			// 	$level_insert_id = $db->insert('dep_user', $insert);
			// }
			//

		}
		$result = $this->result($arr);
		return $response->withJson($result);
	}

	public function director ($request, $response, $args) {
		$db = $this->db;

		$q = 'SELECT DATE_FORMAT(datetime1,"%m") mymonth, action.task_id, label, task.info,task.deadline,
		action.user_id,  user.name, COUNT(action.user_id) user_id_cnt
		FROM user, action
		LEFT JOIN task ON task.task_id=action.task_id
		WHERE action.user_id=user.user_id and datetime1>="2017-09-01 00:00:00"
		GROUP BY mymonth, action.user_id, task_id
		ORDER BY task_id,user_id_cnt DESC';
		$tasksbyusers = $db->rawQuery($q);

		foreach ($tasksbyusers as $key => $val) {

			if ($val['task_id']=='') {
				$task_id = '0';
				$val['label'] = 'Обычные события';
				$val['info'] = 'Регулярные события, должностные обязанности, без конкретной задачи руководства';
			} else{
				$task_id = $val['task_id'];
			}



			$out[$task_id]['label'] = $val['label'];
			$out[$task_id]['task_id'] = $task_id;
			$out[$task_id]['info'] = $val['info'];
			$out[$task_id]['deadline'] =$val['deadline'];

			if($val['mymonth']=='9') {
				$out[$task_id]['data'][$val['user_id']]['m9'] = $val['user_id_cnt'];
				@$out[$task_id]['colsum9'] += $val['user_id_cnt'];

			}
			if($val['mymonth']=='10') {
				$out[$task_id]['data'][$val['user_id']]['m10'] = $val['user_id_cnt'];
				@$out[$task_id]['colsum10'] += $val['user_id_cnt'];
			}
			if($val['mymonth']=='11') {
				$out[$task_id]['data'][$val['user_id']]['m11'] = $val['user_id_cnt'];
				@$out[$task_id]['colsum11'] += $val['user_id_cnt'];
			}

			$out[$task_id]['data'][$val['user_id']]['user_id'] = $val['user_id'];
			$out[$task_id]['data'][$val['user_id']]['name'] = $val['name'];

			@$out[$task_id]['data'][$val['user_id']]['rowsum'] += $val['user_id_cnt'];


			@$out[$task_id]['count'] += $val['user_id_cnt'];
		// unset($row);
		// unset($row10);unset($row11);unset($row12);
		}

		if ($db->getLastErrno() === 0) {
			$json['success'] = true;
			$json['tasks'] = $out;
			$json['tasksbyusers'] = $tasksbyusers;
		} else {
			$json['success'] = false;
			$json['errinfo'] = $db->getLastError();
		}

		$json['db_debug'] = $db->trace;

		//  print_r($tasksbyusers);
		// print_r($out);
		return $response->withJson($json);
	}
}

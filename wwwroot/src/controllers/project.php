<?php


class Project extends AbstractController
{
    public function get($request, $response, $args)
    {
        // $this->db->where('deleted', 0);
        // $projects = $this->db->map('project_id')->ArrayBuilder()->get('project');
        // $result = $this->result($projects, 'projects');

        $current_user_id = getUserId();
        $current_user_level = getUserLevel();

// стаус ниже менеджера, т.е. простой пользователь
$only_mine='';
if($current_user_level<3){
$only_mine = "AND user_ids LIKE '% $current_user_id %'";
}
// все проекты с количествм стадий, завершенных и общее количество
//
        $q = "SELECT
(SELECT count(*) count_stages FROM project_todo a WHERE a.project_id=c.project_id AND parent_id=0 AND a.deleted=0) count_stages,
(SELECT count(*) count_stages FROM project_todo a WHERE a.project_id=c.project_id AND parent_id<>0 AND a.deleted=0) count_todos,
(SELECT  count(*) count_done_stages FROM project_todo b WHERE b.project_id=c.project_id AND parent_id=0 AND project_status_id=4 AND b.deleted=0) count_done_stages,
		c.*	FROM project c WHERE deleted=0 $only_mine";

// // HACK: // TODO: почему-то не сформировался подмасив как раньше
        $projects['projects'] = $this->db->map('project_id')->ArrayBuilder()->rawQuery($q);




        foreach ($projects['projects'] as $key => $value) {
            $arr_user_ids=array_filter(explode(' ', trim($value['user_ids'])), function ($value) {
                return $value !== '' && $value !== '|';
            });
            array_walk($arr_user_ids,function (&$v){ $v= intval($v);});


//            $projects['projects'][$key]['user_ids'] = array_filter(explode(' ', trim($value['user_ids'])), function ($value) {
//                return $value !== '' && $value !== '|';
//            });
            $projects['projects'][$key]['user_ids'] =$arr_user_ids;

        }
        $projects['projects'] = array_values($projects['projects']); // что-бы json_decode выдал js-массив,а не объект



        $projects['getUserId']= getUserId();
        $projects['getUserLevel']= getUserLevel();
	       $projects['db_debug'] =  $this->db->trace;
//        return $response->withJson($projects,200,JSON_FORCE_OBJECT);
        return $response->withJson($projects);

    }

    public function get_item($request, $response, $args)
    {
        $q = 'SELECT * FROM project where deleted = 0 && project_id = '.$args['id'].';';

//       $projects = $this->db->map('project_id')->ArrayBuilder()->rawQuery($q);
        $projects = $this->db->ArrayBuilder()->rawQuery($q);

        $result = $this->result($projects);
        $response = $response->withJson($result);
        return $response;
    }




    public function add($request, $response, $args)
    {
        $data = array(
          'title' => $request->getParam('title'),
          'info' => $request->getParam('info'),
           );

           if($request->getParam('date_start')) $data['date_start']= $request->getParam('date_start');
           if($request->getParam('deadline')) $data['deadline']= $request->getParam('deadline');

        $user_ids = $request->getParam('user_ids', []);
        $data['user_ids'] = ' '.implode(' ', $user_ids).' |';

        $id = $this->db->insert('project', $data);

        $this->db->where('project_id', $id);
        $item = $this->db->get('project');
        // HACK:
        $item[0]['user_ids'] = $user_ids;

        $event = array('event_type' => 'created', 'event' => 'Создан новый проект', 'user_id' => getUserId(), 'item_id' => $id);
        $this->db->insert('project_history', $event);

        $result = $this->result($id, 'insert_id');
        $result = $this->result($item, 'item', $result);

        return $response->withJson($result);
    }



    public function edit_item($request, $response, $args)
    {
        $arr = array(
          'title' => $request->getParam('title'),
          'info' => $request->getParam('info'),
//          'date_start'=> $request->getParam('date_start'),
//          'deadline'=> $request->getParam('deadline'),

  );
  // 
   if($request->getParam('date_start')) $arr['date_start']= $request->getParam('date_start');
   if($request->getParam('deadline')) $arr['deadline']= $request->getParam('deadline');

  $user_ids = $request->getParam('user_ids', []);
  $arr['user_ids'] = ' '.implode(' ', $user_ids).' |';


        $this->db->where('project_id',$args['id']);
        $sql_result = $this->db->update('project', $arr);
        $result = $this->result($sql_result);

        return $response->withJson($result);
    }

    public function delete_item($request, $response, $args)
    {
        $this->db->where('project_id', $args['id']);
        $delete = $this->db->update('project', array('deleted' => '1'));
        //$delete = $this->db->delete('project');
        $this->db->where('project_id', $args['id']);
        $this->db->update('project_todo', array('deleted' => '1'));
        $result = $this->result($delete);

        $event = array('event_type' => 'deleted', 'event' => 'Проект удален', 'user_id' => getUserId(), 'item_id' => $args['id']);
        $this->db->insert('project_history', $event);

        $result = $this->result(new DateTime(), 'accessTime', $result);

        return $response->withJson($result);
    }

    public function update_status_item($request, $response, $args)
    {
        $status = $request->getHeader('status', $default = [-1])[0];
        if ($status >= 0) {
            $status_list = $this->db->map('project_status_id')->ArrayBuilder()->get('project_status');
            $this->db->where('project_id', $args['id']);
            $project = $this->db->get('project');
            $this->db->where('project_id', $args['id']);
            // return $response->withJson(['status' => ]);
            $update = $this->db->update('project', array('project_status_id' => $status));

            $event = array('event_type' => 'status_changed', 'event' => 'Статус изменен с '.$status_list[$project[0]['project_status_id']].' на '.$status_list[$status], 'user_id' => getUserId(), 'item_id' => $args['id']);
            $this->db->insert('project_history', $event);

            $result = $this->result($status);

            return $response->withJson($result);
        }

        return $response->withJson(['status' => 'false']);
    }

    public function history($request, $response, $args)
    {
        $q = 'SELECT project_history_id, item_id as project_id, project.title as project_title, datetime, project_history.user_id, name, comment, event  FROM `project_history` join user on project_history.user_id = user.user_id join project on item_id = project_id;';
        $result = $this->result($this->db->rawQuery($q));
        $result = $this->result(new DateTime(), 'accessTime', $result);

        return $response->withJson($result);
    }

    public function history_item($request, $response, $args)
    {
        $q = 'SELECT project_history_id, item_id as project_id, project.title as project_title, datetime, project_history.user_id, name, comment, event  FROM `project_history` join user on project_history.user_id = user.user_id join project on item_id = project_id '.'where project_id = '.$args['id'].';';
        $result = $this->result($this->db->rawQuery($q));
        $result = $this->result(new DateTime(), 'accessTime', $result);

        return $response->withJson($result);
    }
}

<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Product extends AbstractController{
	
	public function get ($request, $response, $args) {
		$page = $request->getParam('page')?$request->getParam('page') : 1;
		$per_page = $request->getParam('per_page')?$request->getParam('per_page') : 15;
		$offset = $per_page*($page-1);
		$this->db->where('hide', 0);
		if($request->getParam('filter')) 
			$this->db->where('name', '%'.$request->getParam('filter').'%','LIKE');
		if($request->getParam('sort')) {
			$sort = explode('|', $request->getParam('sort'));
			$this->db->orderBy($sort[0],$sort[1]);
		}
		
		$products = $this->db->withTotalCount()->get('product', Array ($offset, $per_page));

		$result = $this->result($products);
		$result['total'] = $this->db->totalCount;
		$result['per_page'] = $per_page;
		$result['current_page'] = $page;
		$result['last_page'] = $this->db->totalCount/$per_page;
		$result['next_page_url'] = null;
		$result['prev_page_url'] = null;
		$result['from'] = $offset;
		$result['to'] = $offset + $per_page;
		return $response->withJson($result);
	}

	public function get_item ($request, $response, $args) {
		$this->db->where('product_id',$args['id']);
		$query =  $this->db->get('product');
		$product = $query[0] ? $query[0] : null;

		return $response->withJson($product);
	}
}		
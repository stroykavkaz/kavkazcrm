<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Asterisk extends AbstractController {

	public function get ($request, $response, $args) {


		$db = new MysqliDb (Array (
	    'host' => '192.168.1.63',
	    'username' =>'crm2',
	    'password' => 'Paralaxx',
	    'db'=> 'asteriskcdrdb',
	    'prefix' =>''));


		$q="SELECT calldate, src, dst, dcontext, billsec, path, translation
				FROM cdr
				WHERE billsec >0 AND path<>''
				ORDER BY calldate DESC";

		$calls = $db->rawQuery($q);

		foreach ($calls as $key => $val) {
			$calls[$key]['path'] = str_replace('/var/spool/asterisk','http://192.168.1.63',$calls[$key]['path']);
		}

		if ($db->getLastErrno() === 0) {
			$arr['success'] = true;
			$arr['calls'] = $calls;
		} else {
			$arr['success'] = false;
			$arr['errinfo'] = $db->getLastError();
		}


		return $response->withJson($arr);
	}


}

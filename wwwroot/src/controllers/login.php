<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Login extends AbstractController{

	public function index($request,$response, $args)
	{
		$this->login_form($request,$response);
	}

	public function logout($request, $response, $args)
	{  
		setcookie ("token", 0);
		return $response->withRedirect('login?logout');
	}

	public function getform($request, $response, $args)
	{
		echo file_get_contents('templates/login.html');
	}


	public function status($request, $response, $args)
	{
		$db = $this->db;

		$db->where('user_id', decrypt($_COOKIE['token']));
		$user = $db->get('user', null, ['user_id','login','user_level']);
		$data = (isset($user[0])) ? $user[0] : 0 ;
		if($data['user_level']==4) $data['admin']=1;
			// $data = array(
			//         'success' => true,
			//         'admin' => 1,
			//         'login' => 'a.vlasov23',
			//      );
			// $data['cookie'] =  $_COOKIE['token'];
		return $response->withJson($data);
	}

	public function post($request, $response, $args)
	{
		$db = $this->db;

		$db->where('login', $request->getParam('login'));
		$db->where('password', $request->getParam('password'));
		$db->where('hide', '0');
		$user = $db->get('user');

		$user_id = (isset($user[0])) ? $user[0]['user_id'] : 0 ;

		if($user_id) {
			setcookie ("token", encrypt($user_id)); // TODO: через респонс
			echo 'good login';

			return $response->withRedirect('/');
		}else{
			setcookie ("token", 0);
			echo 'bad login';
			return $response->withRedirect('login?bad', 403);
		}
	}
}
<?php

abstract class AbstractController{

	public $db;
	private $container;

	public function __construct($container) {
		$this->container = $container;
		$this->db = $container->get('db');
	}

	protected function result($value,$name = 'data',$arr = null){
		if (!empty($arr)){
			$result = $arr;
			$result[$name] = $value ? $value : new ArrayObject();
			return $result;
		}
		if ($result['success'] = $this->db->getLastErrno() === 0) {
			$result[$name] = $value ? $value : new ArrayObject();
		} else {
			$result['errinfo'] = $this->db->getLastError();
		}
		$result['db_debug'] = $this->db->trace;
		return $result;
	}

	public function get($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function add($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function edit($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function delete($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function head($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function options($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function get_item($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function add_item($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function edit_item($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function delete_item($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function head_item($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public function options_item($request, $response, $args)
	{
		$this->emptyResponse($request, $response, $args);
	}

	public static function emptyResponse($request, $response, $args, $err = null){
		$out['success'] = false;
		$out['error'] = $err ? $err : 'Errorrrr!!!!';
		$out['args'] = $args;

		return $response->withStatus(404)->withJson($out);
	}
}

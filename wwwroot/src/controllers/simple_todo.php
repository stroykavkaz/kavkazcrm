<?php
//INSERT INTO project_todo
//(project_id,parent_id,project_status_id, title, user_ids,children_order_json)
// VALUES
//
// (0,0,1,'Все задачи',37,'[]'),
//             (0,0,2,'Скоро',37,'[]'),
//         (0,0,3,'В работе',37,'[]'),
//              (0,0,4,'Сделано!',37,'[]'),
//  (0,0,0,'Корзина',37,'[]');
    
    
class Simple_todo extends AbstractController
{
  
  public function deadlines($request, $response, $args)
  {
    $q = "SELECT info, user.name,user.surname,deadline, created, info, user_ids
FROM project_todo, user
WHERE project_id = 0 AND deadline IS NOT NULL and user.user_id=user_ids 
AND  parent_id NOT IN (SELECT project_todo_id FROM project_todo WHERE parent_id =0 AND project_id =0 AND ( project_status_id=0 OR  project_status_id=4) ) 
ORDER BY deadline"; 

$result['result'] = $this->db->rawQuery($q);
return $response->withJson($result);

  }
  
  public function mynew($request, $response, $args)
  {
    $q = "SELECT info, user.name,user.surname,deadline,date_finish, created, info, user_ids
FROM project_todo, user
WHERE project_id = 0 AND user.user_id=user_ids 
AND  parent_id NOT IN (SELECT project_todo_id FROM project_todo WHERE parent_id =0 AND project_id =0 AND project_status_id=0 ) 
ORDER BY created DESC"; 

$result['result'] = $this->db->rawQuery($q);
return $response->withJson($result);

  }  
  
  public function all($request, $response, $args)
  {
    $q = "SELECT a.info, a.user_ids, user.name, b.title, b.project_status_id, 
a.created,a.creator_id, a.deadline, a.date_finish
FROM project_todo a, project_todo b, user
WHERE a.project_id = 0 AND user.user_id=a.user_ids AND a.parent_id  = b.project_todo_id
ORDER BY a.created DESC"; 

$result['result'] = $this->db->rawQuery($q);
return $response->withJson($result);

  }  
  
  public function done($request, $response, $args)
  {
    $q = "SELECT info, user.name,user.surname,deadline,date_finish, created, info, user_ids
FROM project_todo, user
WHERE project_id = 0 AND user.user_id=user_ids 
AND  parent_id IN (SELECT project_todo_id FROM project_todo WHERE parent_id =0 AND project_id =0 AND project_status_id=4) 
ORDER BY date_finish DESC"; 

$result['result'] = $this->db->rawQuery($q);
return $response->withJson($result);

  }  

  public function trash($request, $response, $args)
  {
    $q = "SELECT info, user.name,user.surname,deadline,date_finish, created, info, user_ids
FROM project_todo, user
WHERE project_id = 0 AND deadline IS NOT NULL and user.user_id=user_ids 
AND  parent_id IN (SELECT project_todo_id FROM project_todo WHERE parent_id =0 AND project_id =0 AND project_status_id=0 ) 
ORDER BY deadline"; 

$result['result'] = $this->db->rawQuery($q);
return $response->withJson($result);

  }  
    
    public function get($request, $response, $args)
    {
        $user_id = $request->getQueryParam('user_id');

        $this->db->where('user_ids', $user_id);
        $this->db->where('project_id', 0);
        $this->db->where('parent_id', 0);

        $lists = $this->db->get('project_todo');
        $data = [];

        foreach ($lists as $key => $list) {
            $this->db->where('user_ids', $user_id);
            $this->db->where('project_id', 0);
            $this->db->where('parent_id', $list['project_todo_id']); // "дети"
            // $todos = $this->db->map('project_todo_id')->ArrayBuilder()->get('project_todo');
            $todos = $this->db->get('project_todo');

            $data[$list['project_todo_id']]['title'] = $list['title'];
            $data[$list['project_todo_id']]['project_todo_id'] = $list['project_todo_id'];
            $data[$list['project_todo_id']]['children_order_json'] = $list['children_order_json'];
            
            $data[$list['project_todo_id']]['project_status_id'] = $list['project_status_id'];


            $data[$list['project_todo_id']]['todos'] = $todos;
        }

        $result = $this->result($data);

        return $response->withJson($result);
    }

    public function add($request, $response, $args)
    {
      $parent_id = $request->getParam('parent_id');
        $data['project_id'] = 0;
        $data['user_ids'] = $request->getParam('user_ids');
        $data['parent_id'] = $parent_id;
        $data['creator_id'] =getUserId(); 
        $data['info'] = $request->getParam('info');
        
        if($request->getParam('deadline')) $data['deadline'] = $request->getParam('deadline');

        $insert_id = $this->db->insert('project_todo', $data);
        $result = $this->result($insert_id, 'insert_id');
        
    $q = "UPDATE project_todo 
        SET children_order_json = IF(children_order_json = '[]', '[$insert_id]', REPLACE(children_order_json, '[', '[$insert_id,'))         
        WHERE project_todo_id=$parent_id";
        
        $res = $this->db->rawQuery($q);
      
          // отправляем web-push уведомление. только если админ пишет другому юзеру 
        if($data['creator_id']==1 AND $data['user_ids']!=1) { 
//              $onesignal = $this->onesignal($data['info'], $data['user_ids']);
      }

// todo ошибка если не вставилось
    return $response->withJson($result);
    }

    public function options($request, $response, $args)
    {
        $list_json = $request->getParam('lists');
        $moved_id = $request->getParam('moved_id');
        $moved_to_list_id = $request->getParam('moved_to_list_id');
        $moved_to_list_status = $request->getParam('moved_to_list_status');

        $lists = json_decode($list_json, true);

        foreach ($lists as $key => $list) {
            $list_json = json_encode($list);
            $q = "UPDATE project_todo SET children_order_json='$list_json' WHERE project_todo_id='$key';\n"; // сохраняем порядок внутри одного списка
  $res = $this->db->rawQuery($q);
            foreach ($list as $id) { // сохраняем перемещения из списка в список, обновляем всем parent_id. потом оптимизировать, чтобы каждый раз не обновлять всем.
        $q = "UPDATE project_todo SET parent_id='$key' WHERE project_todo_id='$id';\n";
                $res = $this->db->rawQuery($q);
            }

            $res = $this->db->rawQuery($q);
        }

        if($moved_to_list_status == 4) { // перетянули в Сделано -- ставим дату когда сделано
            $qupdate = "UPDATE project_todo SET date_finish=NOW() WHERE project_todo_id='$moved_id';\n";
        }
        else {
            // когда перетягивают ИЗ сделано в другой список, надо очищать дату (типа переделать недоделано)))
            // код будет срабатывать и при других случаях, что не оптимально,
            // но в js пока не понял как получилть код списка из которого тянут.
            $qupdate = "UPDATE project_todo SET date_finish=NULL WHERE project_todo_id='$moved_id';\n";
        }

        $res2 = $this->db->rawQuery($qupdate);

        $result['result'] = $res;

        return $response->withJson($result);
    }
    
    
    public function edit_item($request, $response, $args)
    {
        $todo_id = $args['id'];
        $data['info'] = $request->getParam('info');
        $data['deadline'] = $request->getParam('deadline');
        
        	$this->db->where('project_todo_id',  $todo_id);
          
        $update_result  = $this->db->update('project_todo', $data, 1);

        return $response->withJson($update_result);
    }
    
    
    public function onesignal($msg, $user_id){
  		
  		$title_str = '{{ user_name | default: "Товарищ" }}, новая задача!';
  		
  		$content = array(
  			'en' => $msg,
  			'ru' => $msg
  			);
  			
  			$title = array(
  				'en' => $title_str,
  				'ru' => $title_str
  				);
  		$fields = array(
  			'app_id' => "56b3a8a1-741d-490a-b6cc-754c1e8ac2de",
  			'filters' => array(array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => $user_id)),
  			'data' => array("foo" => "bar"),
        'url' => 'https://' . $_SERVER['SERVER_NAME'] . '/#/simple_todo',
  			'headings' => $title,
  			'contents' => $content
  		);
  		
  		$fields = json_encode($fields);
      // 	print("\nJSON sent:\n");
      // 	print($fields);
  		
  		$ch = curl_init();
  		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
  												   'Authorization: Basic ZjUwMDY4MTMtMzE2Ny00OWYyLTk2NjgtZjIyYTg1NDBmMDlk'));
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  		curl_setopt($ch, CURLOPT_HEADER, FALSE);
  		curl_setopt($ch, CURLOPT_POST, TRUE);
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

  		$response = curl_exec($ch);
  		curl_close($ch);
  		
  		return $response;
  	}
    
}

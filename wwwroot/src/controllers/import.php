<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Import extends AbstractController{
	
	public function get ($request, $response, $args) {
		$result = $this->readProducts('1cn.xml');
		return $response->withJson($result);
	}
	public function readProducts($filename) {
		$ww = implode("", file($filename));
		$xml = simplexml_load_string($ww);
		foreach ($xml as $k => $x)
			$arr[] =  array('id' => get_object_vars($x)['Ид'],'name' => get_object_vars($x)['Наименование'],'unit' => get_object_vars($x)['БазоваяЕдиница'], 'product_group_id' => get_object_vars(get_object_vars($x)['Группы_Уровень1'])['Ид'], 'product_group2_id' => get_object_vars(get_object_vars($x)['Группы_Уровень2'])['Ид'], 'product_group3_id' => get_object_vars(get_object_vars($x)['Группы_Уровень3'])['Ид'], 'product_group4_id' => get_object_vars(get_object_vars($x)['Группы_Уровень4'])['Ид'], 'product_group5_id' => get_object_vars(get_object_vars($x)['Группы_Уровень5'])['Ид']);

		$columns = 'code, name, unit, product_group_id, product_group2_id, product_group3_id, product_group4_id, product_group5_id';
		foreach ($arr as $key => $value) {
			foreach ($value as $key1 => $value1) {
				$arr[$key][$key1] = str_replace("'", "", $arr[$key][$key1]);
			}
			if ($arr[$key]['product_group_id'] == '') $arr[$key]['product_group_id'] = 'NULL';
			if ($arr[$key]['product_group2_id'] == '') $arr[$key]['product_group2_id'] = 'NULL';
			if ($arr[$key]['product_group3_id'] == '') $arr[$key]['product_group3_id'] = 'NULL';
			if ($arr[$key]['product_group4_id'] == '') $arr[$key]['product_group4_id'] = 'NULL';
			if ($arr[$key]['product_group5_id'] == '') $arr[$key]['product_group5_id'] = 'NULL';
			$arr[$key] = "'". implode("', '", $arr[$key]). "'";
			$arr[$key] = str_replace("'NULL'", "NULL", $arr[$key]);

		}
		// echo $arr[$key];
		$values  = implode("), (", $arr);

		$sql = 'INSERT INTO `product` ('.$columns.') VALUES ('.$values.')';
		$result = $sql;
		// $result = $this->db->rawQuery($sql);
		return $result;
	}
}
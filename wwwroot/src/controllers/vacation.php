<?php

use PsrHttpMessageServerRequestInterface as Request;
use PsrHttpMessageResponseInterface as Response;

class Vacation extends AbstractController{
	
	public function get ($request, $response, $args) {

		$this->db->where('deleted', 0);
		if ($request->getParam('user_ids')) $this->db->where('user_vacation.user_id', $request->getParam('user_ids'), 'IN');
		if ($request->getParam('b_date')) $this->db->where('e_date', $request->getParam('b_date'), '>=');
		if ($request->getParam('e_date')) $this->db->where('b_date', $request->getParam('e_date'), '<=');
		// $db->where('dep.dep_id=dep_org.dep_id');
		$this->db->join("user u", "user_vacation.user_id=u.user_id", "LEFT");
		$vacation = $this->db->map('id')->ArrayBuilder()->get('user_vacation');

		$result = $this->result($vacation);

		return $response->withJson($result);
	}

	public function get_item ($request, $response, $args) {

		$this->db->where('id', $args['id']);
		$this->db->join("user u", "user_vacation.user_id=u.user_id", "LEFT");
		$vacation = $this->db->get('user_vacation');

		$result = $this->result($vacation[0]);

		return $response->withJson($result);
	}

	public function add ($request, $response, $args) {

		$data = array('user_id' => $request->getParam('user_id'),'b_date' => $request->getParam('b_date'),'e_date' => $request->getParam('e_date'),'comment' => $request->getParam('comment'));
		$id = $this->db->insert('user_vacation', $data);
		
		$this->db->where('id', $id);
		$this->db->join("user u", "user_vacation.user_id=u.user_id", "LEFT");
		$result = $this->result($this->db->get('user_vacation')[0]);
		$result = $this->result($id,'insert_id',$result);

		return $response->withJson($result);
	}

	public function delete_item ($request, $response, $args) {

		$this->db->where('id', $args['id']);
		$result = $this->db->update('user_vacation',Array ('deleted' => 1));

		return $response->withJson($this->result($result));
	}

}
<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

$container['adb'] = function($c){
	$settings = $c->get('settings')['asterisk'];
	$db = new MysqliDb (Array (
    'host' => $settings['MYSQL_HOST'],
    'username' => $settings['MYSQL_USER'], 
    'password' => $settings['MYSQL_PASSWORD'],
    'db'=> $settings['MYSQL_DATABASE'],
    'prefix' => $settings['MYSQL_PREFIX']));
	$db->setTrace (true); // потом print_r ($db->trace) выведет запросы и время
	return $db;
};

$container['db'] = function($c){
	$settings = $c->get('settings')['db'];
	$db = new MysqliDb (Array (
                'host' => $settings['MYSQL_HOST'],
                'username' => $settings['MYSQL_USER'], 
                'password' => $settings['MYSQL_PASSWORD'],
                'db'=> $settings['MYSQL_DATABASE'],
                'prefix' => $settings['MYSQL_PREFIX']));
	$db->setTrace (true); // потом print_r ($db->trace) выведет запросы и время
	return $db;
};








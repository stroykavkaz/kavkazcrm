<?php

define('JAVADB', 1); // если новая стуктура базы (единая с java CRM)

define('SUBFOLDER', ''); // если в подпапке


define('LEVEL_INTERN', '1'); // видит только свои фирмы, только то, что добавил сам
define('LEVEL_USER', '2'); // чужое только смотрим, нельзя редактировать
define('LEVEL_MANAGER', '3'); // можем редактировать и чужое
define('LEVEL_ADMIN', '4'); // также можем редактировать юзеров

// пока так, потом сделаю отдельную таблицу [HACK]

define('STATUS_CLIENT', '1');
define('STATUS_OLDCLIENT', '2');
define('STATUS_VIPCLIENT', '3');
 define('STATUS_COMPETITOR', '4');
 define('STATUS_SUPPLIER', '5');
 define('STATUS_CLOSED', '6');
 define('STATUS_REFUSED', '7');
 define('STATUS_DIFPROFILE', '8');

 // define('STATUS_PROJECT', '6');

$ORG_STATUS[STATUS_CLIENT]='Клиент';
$ORG_STATUS[STATUS_OLDCLIENT]='Постоянный клиент';
$ORG_STATUS[STATUS_VIPCLIENT]='VIP-клиент';
$ORG_STATUS[STATUS_COMPETITOR]='Конкурент';
$ORG_STATUS[STATUS_SUPPLIER]='Поставщик';
$ORG_STATUS[STATUS_CLOSED]='Закрыт';
$ORG_STATUS[STATUS_REFUSED]='Отказ';
$ORG_STATUS[STATUS_DIFPROFILE]='Другой профиль';

// $ORG_STATUS[STATUS_PROJECT]='Проект';

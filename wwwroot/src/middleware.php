<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(function ($request, $response, $next) {



	if($request->getCookieParam('token') || strstr($request->getServerParams()['REDIRECT_URL'],'login')){
		$response = $next($request, $response);
	}else{
		return $response->withRedirect(SUBFOLDER.'/login', 403);
	}

    return $response;

});

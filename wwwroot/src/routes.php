<?php
// Routes


$app->get('/login', 'Login:getform');//->setName('login_form');
$app->post('/login', 'Login:post');
$app->get('/login/status', 'login:status');
$app->get('/logout', 'Login:logout');

$app->get('/firms', 'Firm:get');
$app->get('/firms/{id}/item', 'Firm:get_item');
$app->post('/firms/{firm_id}/item', 'Action:add');
$app->post('/firms/add', 'Firm:add');
$app->post('/firms/{id}/edit', 'Firm:edit_item');

$app->get('/firms/full_search/[{search_str}]', 'Firm:full_search');

// $app->put('/fav/{id}', 'Firm:fav_item');
// $app->delete('/fav/{id}','Firm:unfav_item');
// $app->get('/search/{search_str}', 'Firm:search');

$app->get('/activity', 'Action:get');
$app->get('/action/{action_id}', 'Action:get_item');
$app->post('/action/{action_id}', 'Action:edit_item');
$app->get('/calendar/{user_id}/{task_id}/{month_year}', 'Action:show');

$app->get('/users', 'User:get');
$app->get('/user/{id}', 'User:get_item');
$app->post('/user', 'User:add');
$app->post('/user/{id}', 'User:edit_item');
$app->get('/director', 'User:director');

$app->get('/tasks', 'Task:get');
$app->post('/tasks', 'Task:add');
$app->post('/tasks/{id}', 'Task:edit_item');

$app->get('/simple_todo/deadlines', 'Simple_todo:deadlines');
$app->get('/simple_todo/done', 'Simple_todo:done');
$app->get('/simple_todo/trash', 'Simple_todo:trash');

$app->get('/stats/user', 'Stats:user');

// $app->get('/call', 'Aster:get1');

$app->get('/deps', 'Dep:get');
// $app->get('/deps/list', 'Dep:get');
$app->post('/dep/add', 'Dep:add');
$app->post('/dep/{id}/edit', 'Dep:edit_item');

$app->get('/city/{country_id}/{search_str}', 'Helper:citySearch');

$app->get('/old2',
    function () {
        echo file_get_contents('templates/app2.html');
    }
);

$app->get('/old',
    function () {
        echo file_get_contents('templates/app.html');
    }
);

$app->get('/',
    function () {
        echo file_get_contents('templates/app_vue.html');
    }
);

$app->any('/{controller}[/{params:.*}]', function ($request, $response, $args) {
	global $app;
	$params = explode('/', $request->getAttribute('params'));
	$args['params'] = $params;
	if (is_numeric($params[0])) {
		$args['id'] = $params[0] + 0;
		if (array_key_exists(1,$params))	$action = $params[1];
	} elseif (array_key_exists(0,$params)) 	$action = $params[0];

	$action_dic = [
		'GET' => 'get',
		'POST' => 'add',
		'PUT' => 'edit',
		'PATCH' => 'edit',
		'DELETE' => 'delete',
		'OPTIONS' => 'options',
		'HEAD' => 'head'
	];
	$args['action'] = empty($action) ? $request->getMethod() : $action; // php7: $args['action'] = $action ?? $request->getMethod();
	$args['action'] = empty($action_dic[$args['action']]) ? $args['action']:  $action_dic[$args['action']];
	$args['action'] = strtolower($args['action']);
	if (!empty($args['id'])) $args['action'] .= '_item';

	$action = $args['action']; // Без переприсвоения не работает?

	if (is_callable(array($args['controller'], $action))) {

		$controller = new $args['controller']($app->getContainer());
		$controller->$action($request, $response, $args);
		// $args = {
		// "controller":<Имя контролера>,
		// "params":[<Список параметров>],
		// "id":<Числовой id, если задан>,
		// "action":<Строка метода>}
	} else {
		AbstractController::emptyResponse($request, $response, $args, 'There is no callable action '. $action. ' for controller '. $args['controller']);
	}
});

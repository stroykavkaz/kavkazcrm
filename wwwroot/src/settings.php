<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        'debug' => true,      // On/Off whoops error
        // 'whoops.editor'       => 'sublime',
        //'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => true,      // Display call stack in orignal slim error when debug is off

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],


        'db' => [
            'MYSQL_HOST'=>'localhost',
            'MYSQL_USER'=>'root',
            'MYSQL_PASSWORD'=>'123',
            'MYSQL_DATABASE'=>'fn1current',
            'MYSQL_PREFIX'=>'',
        ],


        // 'db' => [
        //     'MYSQL_HOST'=>'192.168.1.200',
        //     'MYSQL_USER'=>'jarcrm',
        //     'MYSQL_PASSWORD'=>'df2sga3%Q#fjl',
        //     'MYSQL_DATABASE'=>'jarcrm',
        //     'MYSQL_PREFIX'=>'',
        // ],

       // 'db' => [
       //     'MYSQL_HOST'=>'gators.ru',
       //     'MYSQL_USER'=>'maximjar',
       //     'MYSQL_PASSWORD'=>'maximjar23',
       //     'MYSQL_DATABASE'=>'test100',
       //     'MYSQL_PREFIX'=>'',
       // ],


        // 'asterisk' => [
        //     'MYSQL_HOST'=>'192.168.1.63',
        //     'MYSQL_USER'=>'crm2',
        //     'MYSQL_PASSWORD'=>'Paralaxx',
        //     'MYSQL_DATABASE'=>'asteriskcdrdb',
        //     'MYSQL_PREFIX'=>'',
        // ],
    ],
];

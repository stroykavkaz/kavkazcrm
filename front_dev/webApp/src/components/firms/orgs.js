export default {
  name: 'Orgs',

computed: {
  pages: function () {
    return Math.ceil(this.meta.total / this.search.per_page);
  }
},

  data: function() {

  var  user_id = (this.$store.state.user.user_id==1) ?  0 : this.$store.state.user.user_id;
    // админу по умолчанию выбрано "все пользователи". а другим юзерам -- именно его клиенты.
    return {
      search: {
        search_str: '',
        orderby_field: 'name_short',
        orderby_dir: 'ASC',
        search_city: 0,
        // user_id: -1, // мои направления
          user_id: user_id,
        status_id: 0,
        page: 1,
        dep_ids:0,
        per_page: this.$store.state.user.orgPerPage
      },
        scroll_flag:false, // при возврате с карточки, прокручиваем до этой фирмы только *один раз*
      prev_page:{},
      search_button:true,
        is_loading:false,
      orgs: {},
      meta: {
        pagination: {},
        pre_perpage: 0,
        list_info: '',
        total:'',
        filters: {

        },
        statuses:   this.$store.state.org_statuses,
        search_city: 0
      }
    };
  },
  methods: {
    submitSearch() {
      this.router_goto();
      this.search_button=false;
    },
    clickPrevPage() {

      this.search.page = this.meta.pre_perpage;
      this.router_goto();
    },
    clickNextPage() {

      this.search.page--;
      this.router_goto();
    },
    router_goto() {
      this.$router.push({
        path: 'orgs',
        query: this.search
      });
    },
    enable_search_button(){
      // был глюк с повторным нажатием на кнопку -- если поискоыве параметры не изменились, то vue-router глючил, и потом не срабатывал и на изменение параметров (название, менеджер, город и тд.) поэтому дизаблим кнопку вообще.
        this.search_button=true;
    },
    get_search: function() {

        $.ajax({
            dataType: "json",
            type: "GET",
            data: this.search,
            url: '/firms/full_search/',
            beforeSend: () => this.is_loading = true,
            success: (data) => {
                this.orgs = data.firms
                this.search.list_info = data.list_info

                this.search.pre_perpage = data.pre_perpage

                this.meta.filters.cities = data.filters.cities
                this.meta.filters.users = data.filters.users
                this.meta.total = data.total
                // this.meta.filters.statuses = data.filters.statuses

                this.meta.pre_perpage = data.meta.pre_perpage
                this.search.pre_perpage = data.meta.pre_perpage

                this.scroll_flag = true;
                this.is_loading = false;
            }
        });
    },

perpage_change(){


 this.$store.commit('setOrgPerPage',{perpage:this.search.per_page})
this.get_search();

},
    th_sort(field) {

// первый или клик на другой колонке (а не повторный на той же)
      if (this.search.orderby_field != field)
          this.search.orderby_dir = 'ASC';
    this.search.orderby_field = field;

      if (this.search.orderby_dir == 'DESC') {
        this.search.orderby_dir = 'ASC';
      } else {
        this.search.orderby_dir = 'DESC';
          }

      this.router_goto();

    },

    th_click_sort(event) {

      if (this.search.orderby_field != event.target.dataset.orderby_field) {
        // первый или клик на другой колонке (а не повторный на той же)
        this.search.orderby_dir = 'ASC';
      }

      this.search.orderby_field = event.target.dataset.orderby_field;

      // убираем предыдущие стрелочки-сортировки
      $('.ui.sortable.table th').removeClass('sorted descending ascending')

      if (this.search.orderby_dir == 'DESC') {
        this.search.orderby_dir = 'ASC';
        $(event.target).addClass('sorted ascending');
      } else {
        this.search.orderby_dir = 'DESC';
        $(event.target).addClass('sorted descending');
      }

      this.router_goto();

    },

    td_expand_click(event) {
        // console.log(event)

        // get td parent. Chrome OR FF
      var tr = (event.path)
          ? event.path[2] : event.explicitOriginalTarget.parentElement;


        $(tr).toggleClass('my_disable_ellipsis');
    },

    //highlight search substr
    sh: function(s) {
      var ret = s;

      if (this.search.search_str && ret) {
        var pattern = new RegExp('(' + this.search.search_str + ')', 'i');
        ret = ret.replace(pattern, '<span class="sh_highlight">$1</span>');
      }

      return ret;
    },
    scrollTop(){
      $("html, body").animate({ scrollTop: 0  }, 600);
    },
    isAfter(dateStr){
        return moment().isAfter(dateStr);
    }
  },
  watch: {
    '$route' (to, from) {
      this.get_search(); // TODO 2 раза аякс дергается!
    },
  },
  created: function() {
    // this.search = this.$route.query;
    Object.assign(this.search, this.$route.query);
    this.get_search();

    this.prev_page.page = this.$router.myPreviousRoute.path.split('/')[1];
this.prev_page.id = this.$router.myPreviousRoute.path.split('/')[2];

// показываем / прячем  стрелку быстрой прокрутки вверх
$(window).scroll(function () {
     if ($(this).scrollTop() > 500)  $('.scrollTop').fadeIn();
      else  $('.scrollTop').fadeOut();
 });

  },
  mounted: function() {  },
  updated: function() {
if(this.scroll_flag){
  // зашли на карточку, вернулись назад в список -- и прокручиваем экран до фирмы в которй мы были. без этого было трудно работаь с большими списками.
  if(this.prev_page.page=='firm')
    document.getElementById("firm"+this.prev_page.id).scrollIntoView();
  this.scroll_flag = false;
}
    },

};

export default {
  name: 'Project_todo',
  template: "#tmpl_project_todo",
  data: function() {
    return {
      project: {},
      stages: {},
      newitem: {
        title: '',
        info: '',
        deadline: '',
        date_start: '',
        parent_id: 0,
        user_ids: [],
        hours_estimate: '',
        project_status_id: 1, // "Создано"
        project_id: this.$route.params.id
      },
      vacation_info: {},
      edititem: {
        user_ids: [],
          date_start: '',
          deadline: ''
      },
      delete_modal_msg1: '',
      delete_modal_msg2: ''
    };
  },

  computed: {
    statuses(){
      return this.$store.state.project_todo_statuses
    },
    users(){
      return this.$store.state.users
    },
  },
  created: function() {
      $.getJSON('project/' + this.$route.params.id, (data) => {
        // this.project = data.data[this.$route.params.id];
          this.project = data.data[0];
      });

      // заполняем этапы, если они есть
      // $.getJSON('project_todo/?parent_id=' + this.newitem.project_id, (data) => {
      $.getJSON('project_todo/?project_id=' + this.newitem.project_id, (data) => {
        if (Object.keys(data.data).length > 0) this.stages = data.data;
      });
  },
  mounted: function() {
    var self = this;

    // $('#project_todo_add_users').dropdown({
    //   onChange: function(value, text, $choice) {
    //     // console.log(self.newitem)
    //     self.newitem.user_ids = value;
    //     self.checkVacation();
    //   }
    // });

    $('#project_todo_stages').accordion({
      exclusive: false
    });

    // $('#project_todo_comments').accordion();

  },
  methods: {
    // calendarClickEvent: function(event1, jsEvent1, pos1) {
    //     // console.log(event1);
    //     $('html, body').animate({
    //         scrollTop: $("#stage" + event1.event_id).offset().top
    //     }, 200, function() {
    //
    //         // $('#project_todo_stages').accordion('close others');
    //         $('#project_todo_stages').accordion('open', event1.key_id);
    //     });
    //
    // },
    // calendarEvents: function() {
    //     var arr = [];
    //     var i = 0;
    //     $.each(this.stages, function(key, val) {
    //         event_info = {
    //             event_id: val.project_todo_id,
    //             key_id: i++,
    //             title: val.title,
    //             start: val.date_start,
    //             end: val.deadline,
    //             cssClass: 'project_todo__status_id' + val.project_status_id,
    //         }
    //         arr.push(event_info)
    //     })
    //
    //     return arr;
    // },
    checkVacation: function() {
      if (this.newitem.deadline && this.newitem.user_ids.length) {
        var data = {
          'user_ids': this.newitem.user_ids,
          'e_date': this.newitem.deadline
        }
        $.getJSON('vacation', data, (data) => this.vacation_info = data.data);
      } else {
        this.vacation_info = {}
      }
    },

    addItem: function() {
      $.post(
        'project_todo', //url
        this.newitem, //data
        (success) => { //callback
          // parent_id<>0 значит это подзадача, parent_id=0 значит это стадия
          var collection = this.newitem.parent_id ? this.stages[this.newitem.parent_id].todos : this.stages;

          // console.log(success)
          this.$set(collection, success.insert_id, success.item[success.insert_id]);


          // типа reset  формы
          this.newitem.title = '';
          this.newitem.info = '';
          this.newitem.parent_id = 0;
          this.newitem.user_ids = [];
          this.newitem.deadline = '';
          this.newitem.date_start = '';
          this.newitem.hours_estimate = '';


          // т.к. семантик-компоненты не дружат с vuejs, и про обновление модели не знают, приходится им делать такое
          // $('#project_todo_add_parent').dropdown('restore defaults');
          // $('#project_todo_add_parent').dropdown('refresh');
          // $('#project_todo_add_users').dropdown('clear'); // типа reset  формы
          // $('#project_todo_add_calendar_start').calendar('clear');
          // $('#project_todo_add_calendar_finish').calendar('clear');

          // закрываем попап
          $('#project_todo_addform_modal').modal('hide');

        }, 'json'
      );
    },
    removeItem: function(stage_id, todo_id = 0) {
      var self = this;
      if (todo_id) {
        var item_type = 'задачу';
        var item_col = this.stages[stage_id].todos;
        var delete_item_id = todo_id;
      } else {
        var item_type = 'этап';
        var item_col = this.stages;
        var delete_item_id = stage_id;
      }

      // подсказка, что именно удаляем, для попапа
      this.delete_modal_msg1 = item_col[delete_item_id].title;
      this.delete_modal_msg2 = item_col[delete_item_id].info;

      $('#project_todo_delete_modal')
        .modal({
          onApprove: function() {
            $.ajax({
              url: 'project_todo/' + delete_item_id,
              type: 'DELETE',
              success: (result) => {
                self.$delete(item_col, delete_item_id); // удаляем из модели
              }
            });
          }
        })
        .modal('show');



    },
    editItemModal: function(stage_id, todo_id = 0) {
      var self = this;

      if (todo_id) { // редактируем подзадачу
        Object.assign(this.edititem,this.stages[stage_id].todos[todo_id])
      } else { // редактируем этап
        Object.assign(this.edititem,this.stages[stage_id])
      }


      $('#project_todo_editform_modal').modal({
        onVisible: function() {
          // $('#project_todo_edit_users').dropdown() // пришлось ставить сюда на onVisible, просто ниже, как календарики, дропдаун срабатывал частично (не выбирал уже выбранное)
        },
        onHide: function() {
          //  alert('onHide!')
        }

      }).modal('show');


      // $('#project_todo_edit_calendar_start').calendar({
      //   type: 'date',
      //   //  initialDate: '1981-03-06',
      //   endCalendar: $('#project_todo_edit_calendar_finish'),
      //   onChange: function(date, text) {
      //     // пришлось добавить if(date) потому что видимо при первоначальном создании он вызывает этот метод, и ни к чему хорошему                   это не приводит))
      //     if (date) self.edititem.date_start = moment(date).format('YYYY-MM-DD');
      //     self.checkVacation();
      //   },
      // });
      // $('#project_todo_edit_calendar_start').calendar('set date', this.edititem.date_start);
      //


      // $('#project_todo_edit_calendar_finish').calendar({
      //   type: 'date',
      //   // initialDate: '1971-03-06',
      //   startCalendar: $('#project_todo_edit_calendar_start'),
      //   onChange: function(date, text) {
      //     if (date) self.edititem.deadline = moment(date).format('YYYY-MM-DD');
      //     self.checkVacation();
      //   },
      // });
      //
      // $('#project_todo_edit_calendar_finish').calendar('set date', this.edititem.deadline);

    },
    editItemModalSubmit: function() {
      var self = this
      $.ajax({
        method: "PUT",
        url: 'project_todo/' + this.edititem.project_todo_id,
        data: this.edititem,
        dataType: "json",
        success: function(data, status) {
          if (data.success) { //(data.success)
            $('#project_todo_editform_modal').modal('hide')
            if(data.data.parent_id)  
              self.$set(self.stages[data.data.parent_id].todos,data.data.project_todo_id,data.data)
            else
              self.$set(self.stages,data.data.project_todo_id,data.data)
          } else {
            alert('Ошибка отправки!')
          }
        }
      });

    },
    changeStatus: function(status_id, todo_id, $event) {
      // console.log(status_id, todo_id, $event)
      $.post('project_todo/' + todo_id + '/update_status', {
        "status": status_id
      });
    },
    showAddModal: function(stage_id = 0) {
      var self = this;

      this.newitem.parent_id = stage_id;

      $('#project_todo_addform_modal').modal({}).modal('show');


      // календарь что-то не подцеплялся при инициализации в mounted, поэтому перенес сюда. потом разобратся
      // $('#project_todo_add_calendar_start').calendar({
      //   type: 'date',
      //   endCalendar: $('#project_todo_add_calendar_finish'),
      //   onChange: function(date, text) {
      //     self.newitem.date_start = moment(date).format('YYYY-MM-DD');
      //     self.checkVacation();
      //   },
      // });
      // $('#project_todo_add_calendar_start').calendar('set date', (new Date()));



      // $('#project_todo_add_calendar_finish').calendar({
      //   type: 'date',
      //   startCalendar: $('#project_todo_add_calendar_start'),
      //   onChange: function(date, text) {
      //     self.newitem.deadline = moment(date).format('YYYY-MM-DD');
      //     self.checkVacation();
      //   },
      // });

    },
    getTotalTodos: function(stage_id) {
      if (stage_id) {
        var length = Object.keys(this.stages[stage_id].todos).length;
        return length;
      }

    },
    // подсчет туду по статусам, возвращаем кол-во указанного status_id или ноль
    // getCountByStatus: function(todos, status_id) {
    //     if (todos) {
    //       var  ret =  _.countBy(todos, 'project_status_id')
    //       return ret[status_id] ? ret[status_id] : 0;
    //
    //     }
    // },
    getStatuscount: function(todos, status_id) {
      var ret = 0;

      if (todos) {
        for (var item in todos) {
          if (todos[item].project_status_id != undefined) {
            ret = todos[item].project_status_id == status_id ? ret + 1 : ret
          }

        }
      }
      return ret;
    }

  },
  filters: {
    formatDate: function(value) {
      if (!value) return '';
      var ret = moment(value).format('DD-MM-YYYY');
      return ret;
    }
  },
  components: {}

}

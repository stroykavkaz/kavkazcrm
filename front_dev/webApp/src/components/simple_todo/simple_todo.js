export default {

  name: 'Simple_todo',
  // template: "#tmpl_projects",
  //  computed: mapState(['users']),
  computed:{  },
  data: function() {
    return {
      user_id:0,
      show_trashbin:0,
      list_id:0,
      users:this.$store.state.users,
      lists: null,
      moved_id: 0, // последний перетянутый элемент
        moved_to_list: 0, // в какой список перетянули
        moved_to_list_status: 0, // в какой список перетянули, его status_id

        dragging: 0, // вспомогательный флаг, устанавилваем когда перетягиваем
      edititem: {
        id: 0
      },
      activeitem: {
        id: 0
      },
      show_done:'today',
      select_list: 0,
      newitem: {
        title: '',
        info: '',
        deadline: '',
        parent_id: 0,
        user_ids: 0,
        hours_estimate: '',
      },
      sortable_options:{group:'group1',disabled:false},
      statuses: {
        1: {
          status_id: 1,
          status: "Создан",
        },
        2: {
          status_id: 2,
          status: "В работе",
        },
        3: {
          status_id: 3,
          status: "Пауза",
        },
        4: {
          status_id: 4,
          status: "Сделано!",
        },
        5: {
          status_id: 5,
          status: "Отменено",
        },
        6: {
          status_id: 6,
          status: "Проблема",
        }

      },
    };
  },

  watch: {
      '$route':'getLists',
    },
  created() {
      this.getLists();
  },
  mounted() {

    var self = this;
        $('.ui.dropdown').dropdown();
        },
  methods: {

editStart(todo_id){
  this.edititem.id = todo_id;
  this.sortable_options.disabled = true;
},
editEnd(todo){
  var id = todo.project_todo_id;

  $.ajax({
    dataType: "json",
    type: "PUT",
    data: todo,
    url: "simple_todo/"+id,
    success: (data) => {
$('#saved'+id).transition('slide down').transition('fade right','6000ms');
    // alert(data);
      this.edititem.id = 0; // снимаем флаг редактирования
      this.sortable_options.disabled = false;
    }
  });

 },

    changeList(e) {
      var user_id = e.target.selectedOptions[0]._value;
      this.$router.push({ name: 'simple_todo', params: { user_id } });

    },
    getLists() {
    this.user_id =  this.$route.params.user_id ? this.$route.params.user_id:  this.$store.state.user.user_id;

      // console.log(this.current_user);
      $.ajax({
        dataType: "json",
        url: 'simple_todo/?user_id='+this.user_id,
        success: (data) => {
          this.lists = data.data;
          this.sortLists();
        }
      });

    },
    sortLists() {
      // сортируем задачи каждого списка по его полю children_order_json
      for (var i in this.lists) {
        var sorted = [];
        var list = this.lists[i];
        var order = JSON.parse(list.children_order_json);

        order.forEach(function(el, i, arr) {
          list.todos.forEach((el2, i2, arr2) => {
            if (el2.project_todo_id == el) sorted.push(el2);
          });
        })
        this.lists[i].todos = sorted;

      }
    },

    addTodo(list_id) {
      if (this.newitem.info!="") {  // если не пустое сообщение
      this.newitem.user_ids = this.user_id
      this.newitem.parent_id =  list_id // Список "Все задачи" - он первый

      this.lists[list_id].todos.unshift({
        project_todo_id:777, //пока временный номер, после аякс-ответа заменяю "настоящим" из БД
        info: this.newitem.info,
        deadline: this.newitem.deadline,
        parent_id: list_id,
        creator_id: this.$store.state.user.user_id
      });



      $.post('simple_todo', this.newitem, (res) => {
        if (res.success) {
            this.$set(this.lists[list_id].todos, 0, {
            project_todo_id:res.insert_id,
            info: this.newitem.info,
            deadline: this.newitem.deadline,
            parent_id: list_id,
            creator_id: this.$store.state.user.user_id
          })

        // this.lists[list_id].todos[0].project_todo_id = res.insert_id;
        // $('#saved'+res.insert_id).transition('slide down').transition('fade right','6000ms');
          // $('.saved:first').fadeIn();
        this.newitem.info = '';
          this.newitem.deadline = '';
        }
      }, 'json')
      .fail(
    (jqXHR, textStatus, errorThrown) => {
          this.lists[list_id].todos.shift();
          alert('Не получилось отправить на сервер. Проверьте соединение и добавьте задачу еще раз');
     }
 );


    }
    },

    getIdsOrder(list_id) { // порядок задач конкретного списка
      var order = [];
      var todos = this.lists[list_id].todos;
      for (var idx in todos) {
        order.push(todos[idx].project_todo_id);
      }
      return order;
    },

    deadlineClass: function (date) {
      var overdueDays = moment().diff(date, 'days'); // сколько после дедлайна просрочено дней
      var cssClass='';
    if(overdueDays > 7) cssClass ='failed'
    else if(overdueDays >1) cssClass ='danger'
    else if(overdueDays <2 && overdueDays > -3) cssClass ='today'

 return cssClass;
},
    getOrder() { // порядки для всех списков
      var ids = {};
      for (var idx in this.lists) {
        ids[idx] = this.getIdsOrder(idx);
      }
      return ids;
    },
    saveListsOrderRemote(e) {

      this.dragging = 0;

      var data = {};
      data.lists = JSON.stringify(this.getOrder());
      data.moved_id = this.moved_id;
        data.moved_to_list = this.moved_to_list;
        data.moved_to_list_status = this.moved_to_list_status;

        $.ajax({
        url: 'simple_todo',
        data: data,
        method: 'OPTIONS'
      });
    },


    onMove(evt, originalEvent) {
      this.moved_id = evt.draggedContext.element.project_todo_id;
        this.moved_to_list = evt.relatedContext.component.$parent.list_id;
        this.moved_to_list_status = evt.relatedContext.component.$parent.list_status_id;

        console.log('onMove',evt);
    },
    onEnd(evt, originalEvent){
        this.setDoneDate();
        this.saveListsOrderRemote();
        // if(this.moved_to_list_status==4) alert('done!');
    },

      onEndTrash(evt, originalEvent){
          this.setDoneDate();
          this.saveListsOrderRemote();
          // alert('onEndTrash')
      },


      onstart(evt){
    this.dragging = 1;
},
// onchange(evt){
// },
setDoneDate(){

    if (this.lists[this.moved_to_list].project_status_id==4) { // перетянули в "Сделано"

        var item= this.lists[this.moved_to_list].todos.find(el => el.project_todo_id==this.moved_id);
        item.date_finish = moment();

    } else if (this.lists[this.moved_to_list].project_status_id!=4) // если перетянули из "сделано", то нужно снять date_finish
    {
        var item= this.lists[this.moved_to_list].todos.find(el => el.project_todo_id==this.moved_id);
        if(item.date_finish) item.date_finish = '';
    }
},
toggleDone(){
  this.show_done = (this.show_done=='today') ? 'all' : 'today';
},
filterDone(list_id,date_finish){
  if (list_id==4 && this.show_done=='today') { // фильтруем только "Сделано" при "показывать за сегодня"
  return  moment(date_finish).isSame(moment(), 'day'); // сегодня? тогда окай.
}
else return true;
},
  }
};

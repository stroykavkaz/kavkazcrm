// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueEvents from 'vue-events'
import { sync } from 'vuex-router-sync'
import store from './store'

import { DatePicker, Autocomplete, Table, TableColumn, Select,Option } from 'element-ui'
import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'

import VueMoment from '../node_modules/vue-moment/vue-moment.js'


import Multiselect from 'vue-multiselect'
import vSelect from "vue-select"


import '../node_modules/semantic-ui-css/semantic.css'
import '../node_modules/semantic-ui-calendar/dist/calendar.min.css'
import 'element-ui/lib/theme-chalk/index.css'

import '../node_modules/semantic-ui-css/semantic.min.js';
import '../node_modules/semantic-ui-calendar/dist/calendar.min.js';

// import Cookies from "js-cookie";
window.Cookies = require('js-cookie');

import App from './App'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueEvents)
Vue.use(VueMoment)

locale.use(lang)
Vue.use(DatePicker)
Vue.use(Autocomplete)
// Vue.use(Scrollbar)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Multiselect)
Vue.use(Select)
Vue.use(Option)

// Vue.component('multiselect', Multiselect)


// Vue.use(Select)
// Vue.use(Option)

import draggable from 'vuedraggable';
Vue.component('draggable', draggable);



// самописные компоненты
import fullcalendar from './components/FullCalendar.vue'
Vue.component('fullcalendar',fullcalendar)

import multiselect from './components/Multiselect.vue'
Vue.component('multiselect',multiselect)

import calendar from './components/Calendar.vue'
Vue.component('calendar',calendar)

import sortable from './components/sortable.vue'
Vue.component('sortable',sortable)

import trashbin from './components/trashbin.vue'
Vue.component('trashbin',trashbin)

import Gantt from './components/projects/gantt.vue'
Vue.component('gantt',Gantt)


var routes0 = require('./routes_0.js')
var routes1 = require('./routes_1.js')

var routes = routes0.concat(routes1)
const router = new VueRouter({
  routes: routes,
  linkActiveClass:'active',
//   scrollBehavior: (to, from, savedPosition) => {
//   return savedPosition;
// }
})

// сохраняем предыдущий адрес
router.beforeEach((to, from, next) => {
router.myPreviousRoute= from;
next();
})


sync(store, router)
// русификация https://github.com/mdehoog/Semantic-UI-Calendar
 $.fn.calendar.settings.text={
   days: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
   months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
   monthsShort: ['Янв','Фев', 'Мрт', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
   today: 'Сегодня',
   now: 'Сейчас',
   am: 'AM',
   pm: 'PM'
 }




new Vue({
	router,
  store,
  el: '#app',
  template: '<App/>',
  components: { App }
})


// var OneSignal = window.OneSignal || [];
// OneSignal.push(["init", {
//   appId: "56b3a8a1-741d-490a-b6cc-754c1e8ac2de",
//   autoRegister: true,
//   notifyButton: {
//     enable: false /* Set to false to hide */
//   },
//   persistNotification:true,
//   promptOptions:{
//     actionMessage: 'Разрешить приложению KavkazCRM присылать уведомления'
//   },
//   welcomeNotification: {
//       "title": "KavkazCRM",
//       "message": "Вы подписаны на уведомления",
//       // "url": "" /* Leave commented for the notification to not open a window on Chrome and Firefox (on Safari, it opens to your webpage) */
//   }
// }]);


// OneSignal.push(function() {
//
//  OneSignal.sendTags({
//    user_id: window.myglobal.user_id,
//    user_name: window.myglobal.user_name,
//    user_surname: window.myglobal.user_surname,
//  });
// });

import todos_day from './components/todos/todos_day.vue'
import todos_month from './components/todos/todos_month.vue'
import todos_history from './components/todos/todos_history.vue'
import Firms from './components/firms/firms.vue'
import Favorites from './components/firms/favorites.vue'
import Firm from './components/firms/firm.vue'
import Firm_edit from './components/firms/firm_edit.vue'
import Users from './components/users/users.vue'
import User from './components/users/user.vue'
import User_action from './components/users/user_action.vue'
import Vacations from './components/users/vacations.vue'
import Project_history from 'components/projects/project_history.vue'
import Project_estimated from './components/projects/project_estimated.vue'

// import Products from './components/products/products.vue'
// import Products_action from './components/products/product_action.vue'

var routes = [
	{path:'/',redirect:'simple_todo'},
	{path:'/todos/month',component:todos_month},
	{path:'/todos/day',component:todos_day},
	{path:'/todos/history',component:todos_history},
	{path:'/todos/',component:todos_day},
	{path:'/firms',component:Firms},
    {path:'/firms/fav',component:Favorites},
	{path:'/firms/add',component:Firm_edit},
	{path:'/firms/:sort',component:Firms},
	{path:'/firm/:id',component:Firm},
	{path:'/orgs/add',component:Firm_edit},
	{path:'/firm/:id/edit',component:Firm_edit},
	{path:'/users',component:Users},
	{path:'/user/:user_id',component:User, props:true},
	{path:'/users/add',component:User_action},
	{path:'/user/:id/edit',component:User_action},
	{path:'/users/vacations',component:Vacations},
	{path:'/projects/history',component:Project_history},
	{path:'/project/:id/history',component:Project_history},
	{path:'/project/:id/estimated',component:Project_estimated},
	// {path:'/products/',component:Products},
	// {path:'/products/add',component:Products_action},
	// {path:'/product/:id/edit',component:Products_action},

]


module.exports = routes

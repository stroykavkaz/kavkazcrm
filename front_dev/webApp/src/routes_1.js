import Projects from './components/projects/projects.vue'
import Project_todo from './components/project_todo/project_todo.vue'
import Orgs from './components/firms/orgs.vue'
import Action_edit from './components/actions/action_edit.vue'

import Simple_todo from './components/simple_todo/simple_todo.vue'

import Deps from './components/deps/deps.vue'

// import Deadlines from './components/simple_todo/deadlines.vue'
// import Done from './components/simple_todo/done.vue'
// import Trash from './components/simple_todo/trash.vue'
// import TodoNew from './components/simple_todo/todonew.vue'
import TodoAll from './components/simple_todo/all.vue'

import Calls from './components/calls/calls.vue'

import Push from './components/users/push.vue'
import Stats from './components/director/stats.vue'



var routes = [
  // {path:'/',component:Orgs},

  // {name:'orgs', path:'/orgs/:page_num(\\d+)?/:status_id(\\d+)?/:user_id(\\d+)?/:search_city?/:search_str?',component:Orgs, props: true},
  // {path:'/orgs',component:Orgs, props: {search_str:'xxx',orderby_field:''}},
  // {path:'/orgs',component:Orgs, props: (route)=>{search_query:route.query} },
  {path:'/orgs',component:Orgs},

  {path:'/action/:action_id',component:Action_edit, props: true},

  {path:'/projects',component:Projects},
  {path:'/project/:id',component:Project_todo},
  // {path:'/projects/gantt',component:Gantt},

  // {name: 'deadlines', path:'/simple_todo/deadlines', component:Deadlines},
  // {name: 'done', path:'/simple_todo/done', component:Done},
  // {name: 'trash', path:'/simple_todo/trash', component:Trash},
  // {name: 'todonew', path:'/simple_todo/new', component:TodoNew},
  {name: 'todoall', path:'/simple_todo/all', component:TodoAll},
  
  {name: 'calls', path:'/calls', component:Calls},
  
  
  {name: 'simple_todo', path:'/simple_todo/:user_id?', component:Simple_todo},
  {name: 'push', path:'/push', component:Push},

  
  // {path:'/simple_todo/:user_id', component:Simple_todo},
  
  // {name: 'simple_todo', path:'/simple_todo/:user_id?/:list_id?', component:Simple_todo, props:true},


  {path:'/deps',component:Deps},
    {path:'/stats',component:Stats},

]
module.exports = routes

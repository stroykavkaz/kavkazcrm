import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import users from './modules/users'
import deps from './modules/deps'
import org_statuses from './modules/org_statuses'
import action_types from './modules/action_types'

import project_todo_statuses from './modules/project_todo_statuses'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    user,
    users,
    deps,
    project_todo_statuses,
    org_statuses,
      action_types,
  },
  strict: false,
})

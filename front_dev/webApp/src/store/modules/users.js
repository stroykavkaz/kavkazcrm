import Vue from 'vue'
const state = {}

// getters
const getters = {
	users: state => state
}
// actions
const actions = {
	loadUsers ({ commit, state }) {
		$.getJSON('users', (data) => commit('setUsers', data.users))
	}
}
const mutations = {
	setUsers (state,data){
		for(var key in data){
			Vue.set(state,key,data[key])
		}
	},

}
export default {
	state,
	getters,
	actions,
	mutations
}

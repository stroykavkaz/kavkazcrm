import Vue from 'vue'
const state = {}

// getters
const getters = {
	org_statuses: state => state
}
// actions
const actions = {
	loadOrg_statuses ({ commit, state }) {
		$.getJSON('firm/getStatuses',(data) => commit('setOrgStatuses', data))
	}
}
const mutations = {
	setOrgStatuses (state,data){
		for(var key in data.data){
			Vue.set(state,key,data.data[key])
		}
	},

}
export default {
	state,
	getters,
	actions,
	mutations
}

import Vue from 'vue'
const state = {}

// getters
const getters = {
	Deps: state => state.deps
}
// actions
const actions = {
	loadDeps ({ commit, state }) {
		$.getJSON('dep/',(data) => {
			commit('setDeps', data.deps)
		})
	}
}
const mutations = {
	setDeps (state, deps) {
		for(var key in deps){
			Vue.set(state,key,deps[key])
		}
	},

}
export default {
	state,
	getters,
	actions,
	mutations
}

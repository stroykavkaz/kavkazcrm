import Vue from 'vue'
const state = {
   orgPerPage: 60
}

// getters
const getters = {
  userData: state => state,

}
// actions
const actions = {
  loadUserData({  commit,  state }) {
  $.ajax({
      url: 'user/0',
      dataType: 'json',
      async: false, // // HACK: async is BAD PRACTICE!
      success: (data) => {
        // HACK записываю данные логина глобально. понадобилось для Onesignal Web Push
        window.myglobal = {
          'user_id': data.data.user_id,
          'user_name': data.data.name,
          'user_surname': data.data.surname,
          'user_login': data.data.login,
        };

        return commit('setUserData', data.data);
      }
    })

  }
}
const mutations = {

  setUserData(state, user) {
    for (var key in user) {
      Vue.set(state, key, user[key])
    }
  },
  loadOrgPerPage(state) {
    var n = Cookies.get('orgPerPage') || 60;
    Vue.set(state, 'orgPerPage', n)

  },
  setOrgPerPage(state, payload) {
    Cookies.set('orgPerPage', payload.perpage);
    Vue.set(state, 'orgPerPage', payload.perpage)
  }

}
export default {
  state,
  getters,
  actions,
  mutations
}

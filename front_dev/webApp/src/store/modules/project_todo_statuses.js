import Vue from 'vue'
const state = {
        1: {
          status_id: 1,
          status: "Создан",
        },
        2: {
          status_id: 2,
          status: "В работе",
        },
        3: {
          status_id: 3,
          status: "Пауза",
        },
        4: {
          status_id: 4,
          status: "Сделано!",
        },
        5: {
          status_id: 5,
          status: "Отменено",
        },
        6: {
          status_id: 6,
          status: "Проблема",
        }

      }

// getters
const getters = {
	project_todo_statuses: state => state
}
// actions
const actions = {
	load_project_todo_statuses ({ commit, state }) {
	}
}
const mutations = {
	set_project_todo_statuses (state,data){
		for(var key in data.data){
			Vue.set(state,key,data.data[key])
		}
	},

}
export default {
	state,
	getters,
	actions,
	mutations
}

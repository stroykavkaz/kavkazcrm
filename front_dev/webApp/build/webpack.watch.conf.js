var path = require('path')
var config = require('../config')
var utils = require('./utils')
var webpack = require('webpack')
var merge = require('webpack-merge')
var baseWebpackConfig = require('./webpack.base.conf')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var env = config.watch.env

// const Jarvis = require('webpack-jarvis');


var webpackConfig = merge(baseWebpackConfig, {
  module: {
    loaders: utils.styleLoaders({ sourceMap: config.watch.productionSourceMap, extract: true })
  },
  devtool: config.watch.productionSourceMap ? '#source-map' : false,
  output: {
    path: config.watch.assetsRoot,
    filename: utils.assetsPath('js/[name].js'),
    chunkFilename: utils.assetsPath('js/[id].js')
  },
  vue: {
    loaders: utils.cssLoaders({
      sourceMap: config.watch.productionSourceMap,
      extract: true
    })
  },
  plugins: [
    // new Jarvis({
    //   port: 1337 // optional: set a port
    // }),

  new webpack.ProvidePlugin({
      // jquery
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      // semantic-ui
        moment: 'moment',
        semantic: 'semantic-ui-css',
      Semantic: 'semantic-ui-css',
      'semantic-ui': 'semantic-ui-css'
}),
    new webpack.DefinePlugin({
      'process.env': env
    }),

    new ExtractTextPlugin(utils.assetsPath('css/[name].css')),

    new HtmlWebpackPlugin({
      filename: config.watch.index,
      template: 'index.html',

      chunksSortMode: 'dependency'
    }),

  ]
})


module.exports = webpackConfig

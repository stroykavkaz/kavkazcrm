var Todos = {
	name: 'Todos',
	template: "#tmpl_todos",
	data: function() {
		return {
				todos:{},
				sortDir:0,
			};
	},

	created: function() {
		this.get();
	},
	mounted: function(){
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,listWeek'
			},
			defaultDate: '2016-12-12',
			locale:'ru',
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				{
					title: 'All Day Event',
					start: '2016-12-01'
				},
				{
					title: 'Long Event',
					start: '2016-12-07',
					end: '2016-12-10'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2016-12-09T16:00:00'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2016-12-16T16:00:00'
				},
				{
					title: 'Conference',
					start: '2016-12-11',
					end: '2016-12-13'
				},
				{
					title: 'Meeting',
					start: '2016-12-12T10:30:00',
					end: '2016-12-12T12:30:00'
				},
				{
					title: 'Lunch',
					start: '2016-12-12T12:00:00'
				},
				{
					title: 'Meeting',
					start: '2016-12-12T14:30:00'
				},
				{
					title: 'Happy Hour',
					start: '2016-12-12T17:30:00'
				},
				{
					title: 'Dinner',
					start: '2016-12-12T20:00:00'
				},
				{
					title: 'Birthday Party',
					start: '2016-12-13T07:00:00'
				},
				{
					title: 'Click for Google',
					url: 'http://google.com/',
					start: '2016-12-28'
				}
			]
		});
	}, 
	methods: {

		get: function(){
			data = {'level':'1','user_id':'0','parent_id':'-1'}
			$.getJSON('project_todo',data, (data) => this.todos = Object.keys(data.data).map(key => data.data[key]));
			console.log(data.data)
		},
		sort: function (prop) {
			var self = this
			this.todos = this.todos.sort(function(a, b) {
				ap = a[prop] ? a[prop] : 0;
				bp = b[prop] ? b[prop] : 0;
				dir = self.sortDir * 2 - 1;
				return (ap > bp ? dir : (ap < bp) ? -dir : 0);
			});
			self.sortDir = !self.sortDir
		},
	}
};


// вешаем компонент на путь
routes.push({
	path: '/todos',
	component: Todos
});
routes.push({
	path: '/',
	component: Todos
});

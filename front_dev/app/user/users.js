var Users = {
	name: 'Users',
	template: "#tmpl_users",
	data: function() {
		return {
			users:{}
		};
	},

	created: function() {
		this.get();
	},
	methods: {
		get: function(){
			data = {}
			$.getJSON('users',data, (data) => this.users = data.users)
		}
	}
};


// вешаем компонент на путь
routes.push({
	path: '/users',
	component: Users
});
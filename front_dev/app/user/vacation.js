var Vacation = {
	name: 'Vacation',
	template: "#tmpl_vacation",
	data: function() {
		return {
				vacations:{},
				newitem:{},
				users: {},
				project_info:{},
			};
	},

	created: function() {
		$.getJSON('users', (data) => {
			this.users = data.users
			this.get();
		});
		
	},
	methods: {


		get: function(){
			data = {}
			$.getJSON('vacation',data, (data) => this.vacations = data.data);
		},
		showAddModal: function(stage_id = 0) {
			var self = this;

			$('#vacation_addform_modal').modal('show');
			$('#vacation_add_users').dropdown({
				onChange: function(value, text, $choice) {
					self.newitem.user_id = value;
					self.checkProjects();
				}
			});

			$('#vacation_b_date_calendar').calendar({
				type: 'date',
				endCalendar: $('#vacation_e_date_calendar'),
				onChange: function(date, text) {
					self.newitem.b_date = moment(date).format('YYYY-MM-DD');
					self.checkProjects();
				},
			});
			$('#vacation_e_date_calendar').calendar({
				type: 'date',
				startCalendar: $('#vacation_b_date_calendar'),
				onChange: function(date, text) {
					self.newitem.e_date = moment(date).format('YYYY-MM-DD');
					self.checkProjects();
				},
			});
		},
		addItem: function () {
			var self = this;
			if (this.newitem.b_date && this.newitem.e_date && this.newitem.user_id && this.newitem.comment){
				$.post('vacation',this.newitem,(data) => {
					Vue.set(self.vacations,data.insert_id,data.data);
				},'json');
				$('#vacation_addform_modal').modal('hide');
			}
		},
		checkProjects: function() {
			if (this.newitem.b_date && this.newitem.e_date && this.newitem.user_id){
				data = {'level':1,'project_id':-1,'parent_id':-1,'user_id':this.newitem.user_id,'e_date':this.newitem.e_date,'b_date':this.newitem.b_date}
				$.getJSON('project_todo', data, (data) => this.project_info = data.data);
			} else {
				this.project_info = {}
			}
		},
	}
};


// вешаем компонент на путь
routes.push({
	path: '/users/vacations',
	component: Vacation
});

routes.push({
	path: '/user/:id/vacation',
	component: Vacation
});
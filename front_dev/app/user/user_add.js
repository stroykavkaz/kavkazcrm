var User_add = {
	name: 'User_add',
	template: "#tmpl_useradd",
	data: function() {
		return {
				user:{},
			};
	},

	created: function() {
		if (this.$route.params.id) $.getJSON('user/'+this.$route.params.id, (data) => this.user = data);
	},
	methods: {
		
		postItem: function () {
			var self = this;
			if (this.$route.params.id)	
				$.post('user/'+this.$route.params.id+'/edit',this.user,(data) =>{
				data.success ? self.$router.push('/users') : alert(data.errinfo);
			} ,'json');
			else $.post('user/',this.user,(data) => {
				data.success ? self.$router.push('/users') : alert(data.errinfo);
			},'json')
			
		},
	}
};

routes.push({
	path: '/users/add',
	component: User_add
});

routes.push({
	path: '/user/:id/edit',
	component: User_add
});
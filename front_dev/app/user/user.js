var User = {
	name: 'User',
	template: "#tmpl_user",
	data: function() {
		return {
				user:{},
			};
	},
	created: function() {
		$.getJSON('user/'+this.$route.params.id, (data) => this.user = data);
	},
};

routes.push({
	path: '/user/:id',
	component: User
});
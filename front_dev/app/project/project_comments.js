var Project_comments = {
	name: 'Project_comments',
	template: "#tmpl_project_comments",
	props: ['project_id'],
	data: function() {
		return {
			comments:Object,
			newitem:{
				content:'',
				title:''
			}
		};
	},
	created: function () {
		$.getJSON('project_comment?project_id='+this.$parent.$route.params.project_id, (data) => this.comments = data.data);
	},
	methods:{
		addItem: function() {

			$.post('project_comment/add?project_id='+this.project_id,this.newitem ,(data) => {
				Vue.set(this.comments,data.data.id,data.data)
			},'json');
			this.newitem.content = ''
			this.newitem.title = ''

		}
	},
	filters: {
			formatDate: function(value) {
					if (!value) return '';
					ret = moment(value).format('DD-MM-YYYY');
					return ret;
			}
	}
};

Vue.component('project_comments', Project_comments)

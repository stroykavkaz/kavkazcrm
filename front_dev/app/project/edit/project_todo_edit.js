var Project_todo_edit = {
    name: 'project_todo_edit',
    template: '#tmpl_project_todoedit',
    props: ['_title', '_info','_parent_id','_todo_id'],
    data: function() {
        return {
            title: this._title,
            info: this._info,

        };
    },
    methods: {
        update: function() {
            console.log(this.$data);
            this.$emit('input', this.$data);
        },
        delItem: function() {
            console.log('delItem method');
        },
    }
};

Vue.component('Project_todo_edit', Project_todo_edit);

var Project_history = {
    name: 'Project_history',
    template: "#tmpl_project_history",
    data: function() {
        return {
            history:{
            },
            accessTime:0,
        };
    },

    created: function (){
        this.getData()
    },
    methods:{
        getData: function() {
            id = this.$parent.$route.params.project_id
            url = id ? 'project/'+id+'/history' : 'project/history'
            $.getJSON(url,this.accessTime, (data) => {
                this.history = data.data
                this.sort('datetime',false)
                this.accessTime = data.accessTime
            });
        },
        sort: function (prop, asc) {
                this.history = this.history.sort(function(a, b) {
                if (asc) {
                    return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
                } else {
                    return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
                }
            });
        },   
    }   
};

routes.push({
    path: '/projects/history',
    component: Project_history
});
routes.push({
    path: '/project/:project_id/history',
    component: Project_history
});

var Project_estimated_time = {
    name: 'Project_estimated_time',
    template: "#tmpl_project_estimated_time",
    data: function() {
        return {
        	project: {},
        	stages: {
                todos: {}
            },
            users: {},
        }
    },
    created: function() {
		var self = this

		 $.getJSON('users', (data) => {
            this.users = data.users

            $.getJSON('project/' + this.$route.params.id, (data) => {
                this.project = data.data[this.$route.params.id];
            });

            $.getJSON('project_todo/?project_id=' + this.$route.params.id, (data) => {
                if (Object.keys(data.data).length > 0) {this.stages = data.data;}
                for (stage in self.stages){
                	
                	for(todo in self.stages[stage].todos){
                		var cost_estimated = 0
                		var cost = 0
                		for (user in self.stages[stage].todos[todo].user_ids){
                			cost = cost + self.stages[stage].todos[todo].hours_spent * self.users[self.stages[stage].todos[todo].user_ids[user]].hour_rate
                			cost_estimated = cost_estimated + self.stages[stage].todos[todo].hours_estimate * self.users[self.stages[stage].todos[todo].user_ids[user]].hour_rate
                		}
                		self.$set(self.stages[stage].todos[todo],'cost_estimated',cost_estimated)
                		self.$set(self.stages[stage].todos[todo],'cost',cost)
                	}
                }
            });

        });
    },
    mounted:function(){
    	$('#project_todo_stages').accordion({
            exclusive: false
        });
    },
    filters: {
        formatDate: function(value) {
            if (!value) return '';
            ret = moment(value).format('DD-MM-YYYY');
            return ret;
        }
    },
}

routes.push({
    path: '/project/:id/estimated',
    component: Project_estimated_time
});

var Projects = {
    name: 'Projects',
    template: "#tmpl_projects",

    data: function() {
        return {
            projects: {},
            users: {},
            edititem: {
              title: '',
              info: '',
              date_start: '',
              deadline: '',
              user_ids: []
            },
            newitem: {
                title: '',
                info: '',
                date_start: '',
                deadline: '',
                count_done_stages:0,
                count_stages:0,
                count_todos:0,
                user_ids: []
            },
            statuses: {
                1: {
                    status_id: 1,
                    status: "Создан",
                },
                2: {
                    status_id: 2,
                    status: "В работе",
                },
                3: {
                    status_id: 3,
                    status: "Пауза",
                },
                4: {
                    status_id: 4,
                    status: "Сделано!",
                },
                5: {
                    status_id: 5,
                    status: "Отменено",
                },
                6: {
                    status_id: 6,
                    status: "Проблема",
                }

            },
        };
    },
    beforeCreate: function() {
        // $.getJSON('users', (data) => this.users = data.users);

    },

    created: function() {
        $.ajax({
            url: 'users',
            async: false,
            success: (data) => this.users = data.users
        });

        $.getJSON('project', (data) => this.projects = data.projects);

    },
    mounted: function() {
        var self = this;

        $('#project_addform_dropdown_users').dropdown({
            onChange: function(value, text, $choice) {
                self.newitem.user_ids = value;
            }
        });

        $('.ui.accordion').accordion({
            onOpen: function() {
                // console.log(2134);
                $('.ui.progress').progress();
            }
        });



    },
    methods: {
        showAddModal: function() {
            var self = this;

            $('#projects_addform_modal').modal('show');

            $('#project_addform_calendar_deadline').calendar({
                type: 'date',
                startCalendar: $('#project_addform_calendar_date_start'),
                onChange: function(date, text) {
                    if(date)   self.newitem.deadline = moment(date).format('YYYY-MM-DD');
                }
            });

            $('#project_addform_calendar_date_start').calendar({
                type: 'date',
                // initialDate:(new Date()), // почему-то не работает
                endCalendar: $('#project_addform_calendar_deadline'),
                onChange: function(date, text) {
                    if(date)   self.newitem.date_start = moment(date).format('YYYY-MM-DD');
                },
            });
            $('#project_addform_calendar_date_start').calendar('set date', (new Date()));



        },
        addItem: function() {

            $.post(
                'project', this.newitem,
                (success) => {
                    Vue.set(this.projects, success.insert_id, success.item[0]);
                    // console.log(success.insert_id)
                    this.newitem.info = '';
                    this.newitem.title = '';
                    this.newitem.deadline = '';
                    this.newitem.date_start = '';
                    this.newitem.user_ids = [];
                    $('#project_addform_dropdown_users').dropdown('clear'); // типа reset  формы
                    $('.ui.calendar').calendar('clear'); // типа reset  формы

                    // закрываем попап
                    $('#projects_addform_modal').modal('hide');



                }, 'json'
            );

        },
        removeItem: function(index) {
            if (confirm('Удалить проект "' + this.projects[index].title + '"?')) {
                $.ajax({
                    url: 'project/' + index,
                    type: 'DELETE',
                    success: (result) => {
                        Vue.delete(this.projects, index); // удаляем из модели
                    }
                });
            }

        },
        editItemModal: function(project_id) {

            this.edititem = this.projects[project_id];


            $('#projects_edit_modal').modal({
                onVisible: function() {
                    $('#project_editform_dropdown_users').dropdown() // пришлось ставить сюда на onVisible, просто ниже, как календарики, дропдаун срабатывал частично (не выбирал уже выбранное)
                }
            }).modal('show');


            $('#project_editform_calendar_deadline').calendar({
                type: 'date',
                startCalendar: $('#project_editform_calendar_date_start'),
                onChange: (date, text) => {
                    if(date)  this.edititem.deadline = moment(date).format('YYYY-MM-DD');
                }
            });

            $('#project_editform_calendar_deadline').calendar('set date', this.edititem.deadline);


            $('#project_editform_calendar_date_start').calendar({
                type: 'date',
                endCalendar: $('#project_editform_calendar_deadline'),
                onChange: (date, text) => {
                  if(date)  this.edititem.date_start = moment(date).format('YYYY-MM-DD');
                }
            });

            $('#project_editform_calendar_date_start').calendar('set date', this.edititem.date_start);


        },
        editItemModalSubmit: function() {
            $.ajax({
                method: "PUT",
                url: 'project/' + this.edititem.project_id,
                data: this.edititem,
                dataType: "json",
                success: function(data, status) {
                    if (data.success) {
                        $('#projects_edit_modal').modal('hide')

                    } else {
                        alert('Ошибка отправки!')
                    }
                }
            });

        },
        procentDone: function(done, total) {
          var percent =0;
                if (done && total) {

                    percent = Math.round((done / total) * 100);
                } else {
                    percent = 0
                }

                return percent
            }

    },
    filters: {
        formatDate: function(value) {
            if (!value) return '';
            var ret = moment(value).format('DD-MM-YYYY');
            return ret;
        }
    }
};


// вешаем компонент на путь
routes.push({
    path: '/projects',
    component: Projects
});

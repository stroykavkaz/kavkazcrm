var Firm_edit = {
  name: 'Firm_edit',
  template: "#tmpl_firm_edit",
  data: function() {
    return {
      firm:{
        firm_info:{},
      },
    };
  },
  computed: {
    id:function(){
      return this.$route.params.id ? this.$route.params.id : 0
    }
  },
  created: function() {
    $.getJSON('firm/'+this.id,(data) => this.firm = data)
  },
  methods:{
    fav: function() {
      var url = 'firm/'+this.firm.firm_info.org_id
      url += this.firm.firm_info.is_in_fav ? '/unfav' : '/fav'
      $.getJSON(url,(data) => {
        if (data.message) 
          this.firm.firm_info.is_in_fav = !this.firm.firm_info.is_in_fav
      } )
    },
    edit:function() {
      $.getJSON('/firm/'+this.id+'/edit',this.firm.firm_info)
      console.log(this)
      this.$router.go(window.history.back())
    }
  },
}

routes.push({
  path: '/firm/:id/edit',
  component: Firm_edit
});

var Firms = {
	name: 'Firms',
	template: "#tmpl_firms",
	data: function() {
		return {
			firms:{},
			pagination:{},
		};
	},
	created: function() {
		var sort = this.$route.params.sort ? this.$route.params.sort : 'all'
		var offset = this.$route.params.offset ? this.$route.params.offset : 0
		this.get(sort,offset);
	},
	methods: {
		get: function(sort,offset){
			data = {'sort':sort,'page':offset}
			$.getJSON('firm',data,(data) => {
				this.firms =  Object.keys(data.firms).map(key => data.firms[key]);
				this.pagination = {};
				this.pagination.title = data.list_title;
				this.pagination.info = data.list_info;
				this.pagination.perpage = data.perpage;
				this.pagination.prev = data.pre_perpage;
				this.pagination.next = data.next_perpage;
				this.pagination.sort = data.sortby;
			});
		},
		load: function(sort,offset){
			data = {'sort':sort,'page':offset}
			if(!sort) data['sort'] = this.pagination.sort
			if(!offset) data['page'] = this.pagination.pre_perpage
			$.getJSON('firm',data,(data) => {
				for (var index in data.firms) {
					this.firms.push(data.firms[index])
				}
				this.pagination = {};
				this.pagination.title = data.list_title;
				this.pagination.info = data.list_info;
				this.pagination.perpage = data.perpage;
				this.pagination.prev = data.pre_perpage;
				this.pagination.next = data.next_perpage;
				this.pagination.sort = data.sortby;
			});
		},
		fav: function(id) {
			var url = 'firm/'+id
			url += this.firms[id].is_in_fav ? '/unfav' : '/fav'
			$.getJSON(url,(data) => {
				if (data.message) 
					this.firms[id].is_in_fav = !this.firms[id].is_in_fav
			} )
		}
	}
};


// вешаем компонент на путь
routes.push({
	path: '/firms/:sort',
	component: Firms
});

routes.push({
	path: '/firms/:sort/:offset',
	component: Firms
});

routes.push({
	path: '/firms/',
	component: Firms
});
var Firm = {
	name: 'Firm', //1
	template: "#tmpl_firm",
	data: function() {
		return {
			firm:{
				firm_info:{},
			},
			deps:{},
			todos:{},
			action:{
				action_type_id:1,
				txt:'',
				contact_person:'',
				task_id:0,
				future:'',
				org_id:0,
			},
		};
	},
	mounted:function () {
		var self = this
		$('#action_add_calendar').calendar({
			type: 'date',
			onChange: function(date, text) {
				self.action.future = moment(date).format('YYYY-MM-DD');
				self.checkVacation();
			},
		});
	},
	created: function() {
		var id = this.$route.params.id ? this.$route.params.id : 0
		$.getJSON('deps',(data) => this.deps = data.deps)
		data = {'level':'1','user_id':'0','parent_id':'-1'}
		$.getJSON('project_todo',data, (data) => this.todos = Object.keys(data.data).map(key => data.data[key]));
		$.getJSON('firm/'+id,(data) => this.firm = data)
		this.action.org_id = id
	},
	methods:{
		fav: function() {
			var url = 'firm/'+this.firm.firm_info.org_id
			url += this.firm.firm_info.is_in_fav ? '/unfav' : '/fav'
			$.getJSON(url,(data) => {
				if (data.message)
					this.firm.firm_info.is_in_fav = !this.firm.firm_info.is_in_fav
			} )
		},
		addAction:function() {
			$.post('action',this.action,(data)=>this.firm.actions.unshift(data.data),'json')
		}
	},
}

routes.push({
	path: '/firm/:id',
	component: Firm
});

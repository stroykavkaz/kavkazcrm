var Top_nav = {
	name: 'top_nav',
	template: "#tmpl_top_nav",
	data: function() {
		return {
			user:{}
		}
	},
	created: function(){
		$.getJSON('user/0',(data) => {
			console.log(data)
			this.user = data
		});
	},
	mounted: function() {
		$('#top_nav .ui.dropdown').dropdown();
		$('#top_nav .ui.search').search({
			apiSettings: {
				url: 'firm/search?search_str={query}'
			},
			fields: {
				results : 'firms',
				title   : 'name_short',
				url     : 'goto_url'
			},
			minCharacters : 3
		});
	}
}

Vue.component('top_nav', Top_nav)
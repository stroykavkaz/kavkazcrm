
Path.map("#deps").to(function(){
  // $("#menu-deps").addClass("active");

  $.ajax({
    dataType: "json",
    url: 'deps/list',
    // data: data,
    success: function (data) {
      var template = Handlebars.compile( $('#handlebars-deps').html() );
      $("#main_container").html(template(data)).hide();
      $("#main_container").fadeIn(200);
   }
  });
}).exit(leavePanel);


Path.map("#depadd").to(function(){

      var template = Handlebars.compile( $('#handlebars-depadd').html() );
      $("#main_container").html(template('')).hide();
      $("#main_container").fadeIn(200);


        $("#depadd_form").submit(function(e)
        {
          e.preventDefault(); //STOP default action
          var postData = $("#depadd_form").serializeArray();


            $.ajax(
            {
                type: "POST",
                  url : 'dep/add',
                data: postData,
                success:function(data, textStatus, jqXHR)
                {

                  if(data.success===true){
                  $("#depadd_form").highlight(function(){window.location.hash = "#deps";});
                  // window.location.hash = "#deps";
 // function(){   window.location.hash = "#deps";    }
                  }
                  else{ alert("Ошибка внесения в базу!\n"+data.errinfo);}

                },
                error: function(jqXHR, textStatus, errorThrown)
                {

                      alert('Ошибка отправки! Проблемы с сервером или подключением '+textStatus);
                }
            });

        });


}).exit(leavePanel);

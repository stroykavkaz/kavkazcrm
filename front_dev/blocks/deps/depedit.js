Path.map("#dep/:dep_id").to(function() {
    url_params = this.params;

    $.ajax({
        dataType: "json",
        url: 'deps',
        // data: data,
        success: function(data) {

          $.each(data.deps, function( index, value ) {
          if (value.dep_id==url_params.dep_id) data1=value;
          });

            var template = Handlebars.compile($('#handlebars-depedit').html());
            $("#main_container").html(template(data1)).hide();
            $("#main_container").fadeIn(200);
            $('.ui.checkbox').checkbox();

            $("#depedit_form").submit(function(e) {
                e.preventDefault(); //STOP default action
                var postData = $("#depedit_form").serializeArray();

                console.log(postData);
                $.ajax({
                    type: "POST",
                    url: 'dep/' + url_params.dep_id + '/edit',
                    data: postData,
                    success: function(data, textStatus, jqXHR) {

                        if (data.success === true) {
                            $("#depedit_form").highlight(function() {
                                window.location.hash = "#deps";
                            });

                        } else {
                            alert("Ошибка внесения в базу!\n" + data.errinfo);
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                        alert('Ошибка отправки! Проблемы с сервером или подключением ' + textStatus);
                    }
                });

            });

        }
    });

}).exit(leavePanel);

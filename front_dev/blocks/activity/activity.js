// Path.map("#activity/:filter_by(/:month)(/:month)").to(function() {
// // Path.map("#activity").to(function() {
//     url_params = this.params;
//     $("#menu-activity").addClass("active"); //подсвечиваем в главном меню
//
//     $.ajax({
//         dataType: "json",
//         url: 'activity',
//         // data: data,
//         success: function(data) {
//             var template = Handlebars.compile($('#handlebars-activity').html());
//             $("#main_container").html(template(data)).hide();
//              $("#main_container").fadeIn(200);
//               setCssActive();
//
//
//             $('#menu_users .ui.calendar').calendar({
//                 type: 'month',
//                 onChange: function(date, text) {
//                     month = (date.getMonth() + 1) + '-' + date.getFullYear();
//                      window.location.hash = '#activity/month/' + month;
//                     //ajax_show_actions('user',$('#select_user').val(),month);
//
//                     // $('#calendar_users').data('date',month); // записываем в аттрибут, не знаю как лучше прочитать потом значение календаря в выпадающем списке
//                 },
//             });
//
//
//         }
//     });
//
//
// }).exit(leavePanel);


Path.map("#activity(/:month_year)").to(function() {
    url_params = this.params;
    $("#menu-activity").addClass("active"); //подсвечиваем в главном меню

    if (this.params.month_year) {
        myurl = 'activity?month=' + this.params.month_year;

    } else {
        myurl = 'activity';
    }

    $.ajax({
        dataType: "json",
        url: myurl,
        // data: data,
        success: function(data) {
            var template = Handlebars.compile($('#handlebars-activity').html());
            $("#main_container").html(template(data)).hide(); // прячем чтоб потом показать с fadeIn()
            $("#main_container").fadeIn(200);
            setCssActive();
            $('.ui.calendar input').val(myurl);

            $('.ui.calendar').calendar({
                type: 'month',
                onChange: function(date, text) {
                    month = (date.getMonth() + 1) + '-' + date.getFullYear();
                    window.location.hash = '#activity/' + month;
                    // this.value = date;
                },
            });

        }
    });


}).exit(leavePanel);

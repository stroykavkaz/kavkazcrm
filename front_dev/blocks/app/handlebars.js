// handlebars.js helpers
//http://stackoverflow.com/questions/8853396/logical-operator-in-a-handlebars-js-if-conditional

//https://gist.github.com/akhoury/9118682
Handlebars.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  return options.inverse(this);
});

//http://stackoverflow.com/questions/13046401/how-to-set-selected-select-option-in-handlebars-template


  Handlebars.registerHelper('select', function(value, options) {
      // Create a select element
      var select = document.createElement('select');


  // Populate it with the option HTML
  $(select).html(options.fn(this));

  //below statement doesn't work in IE9 so used the above one
  //select.innerHTML = options.fn(this);

      // Set the value
      select.value = value;

      // Find the selected node, if it exists, add the selected attribute to it
      if (select.children[select.selectedIndex]) {
          select.children[select.selectedIndex].setAttribute('selected', 'selected');
      } else { //select first option if that exists
          if (select.children[0]) {
              select.children[0].setAttribute('selected', 'selected');
          }
      }
      return select.innerHTML;
  });


// radio checked
//http://stackoverflow.com/questions/26066768/how-to-set-the-selected-item-in-a-radio-button-group-in-handlebars-template
Handlebars.registerHelper ("setChecked", function (value, currentValue) {
    if ( value == currentValue ) {
       return "checked";
    } else {
       return "";
    }
 });

// https://gist.github.com/akhoury/9118682
 /* a helper to execute an IF statement with any expression
    USAGE:
   -- Yes you NEED to properly escape the string literals, or just alternate single and double quotes
   -- to access any global function or property you should use window.functionName() instead of just functionName()
   -- this example assumes you passed this context to your handlebars template( {name: 'Sam', age: '20' } ), notice age is a string, just for so I can demo parseInt later
   <p>
     {{#xif " name == 'Sam' && age === '12' " }}
       BOOM
     {{else}}
       BAMM
     {{/xif}}
   </p>
   */

  Handlebars.registerHelper("xif", function (expression, options) {
    return Handlebars.helpers["x"].apply(this, [expression, options]) ? options.fn(this) : options.inverse(this);
  });

  /* a helper to execute javascript expressions
   USAGE:
   -- Yes you NEED to properly escape the string literals or just alternate single and double quotes
   -- to access any global function or property you should use window.functionName() instead of just functionName(), notice how I had to use window.parseInt() instead of parseInt()
   -- this example assumes you passed this context to your handlebars template( {name: 'Sam', age: '20' } )
   <p>Url: {{x " \"hi\" + name + \", \" + window.location.href + \" <---- this is your href,\" + " your Age is:" + window.parseInt(this.age, 10) "}}</p>
   OUTPUT:
   <p>Url: hi Sam, http://example.com <---- this is your href, your Age is: 20</p>
  */
 Handlebars.registerHelper("x", function(expression, options) {
   var result;

   // you can change the context, or merge it with options.data, options.hash
   var context = this;

   // yup, i use 'with' here to expose the context's properties as block variables
   // you don't need to do {{x 'this.age + 2'}}
   // but you can also do {{x 'age + 2'}}
   // HOWEVER including an UNINITIALIZED var in a expression will return undefined as the result.
   with(context) {
     result = (function() {
       try {
         return eval(expression);
       } catch (e) {
         console.warn('•Expression: {{x \'' + expression + '\'}}\n•JS-Error: ', e, '\n•Context: ', context);
       }
     }).call(context); // to make eval's lexical this=context
   }
   return result;
});

Path.map("#tasks/:task_id").to(function() {
    url_params = this.params;

    $.ajax({
        dataType: "json",
        url: 'tasks',
        // data: data,
        success: function(data) {

          $.each(data.tasks, function( index, value ) {
          if (value.task_id==url_params.task_id) data1=value;
          });

            var template = Handlebars.compile($('#handlebars-taskedit').html());
            $("#main_container").html(template(data1)).hide();
            $("#main_container").fadeIn(200);
            $('.ui.checkbox').checkbox();

            $('.ui.calendar').calendar({   type: 'date', formatter: {
          date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return year + '-' + month + '-' + day;
          }
          }  });


            $("#taskedit_form").submit(function(e) {
                e.preventDefault(); //STOP default action
                var postData = $("#taskedit_form").serializeArray();

                console.log(postData);
                $.ajax({
                    type: "POST",
                    url: 'tasks/' + url_params.task_id,
                    data: postData,
                    success: function(data, textStatus, jqXHR) {

                        if (data.success === true) {
                            $("#taskedit_form").highlight(function() {
                                window.location.hash = "#tasks";
                            });

                        } else {
                            alert("Ошибка внесения в базу!\n" + data.errinfo);
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                        alert('Ошибка отправки! Проблемы с сервером или подключением ' + textStatus);
                    }
                });

            });

        }
    });

}).exit(leavePanel);

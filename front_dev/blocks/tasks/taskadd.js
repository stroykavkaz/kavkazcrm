
Path.map("#tasks/add").to(function(){

      var template = Handlebars.compile( $('#handlebars-taskadd').html() );
      $("#main_container").html(template('')).hide();
      $("#main_container").fadeIn(200);

      $('.ui.calendar').calendar({   type: 'date', formatter: {
    date: function (date, settings) {
      if (!date) return '';
      var day = date.getDate();
      var month = date.getMonth() + 1;
      var year = date.getFullYear();
      return year + '-' + month + '-' + day;
    }
    }  });
    

        $("#taskadd_form").submit(function(e)
        {
          e.preventDefault(); //STOP default action
          var postData = $("#taskadd_form").serializeArray();


            $.ajax(
            {
                type: "POST",
                  url : 'tasks/add',
                data: postData,
                success:function(data, textStatus, jqXHR)
                {

                  if(data.success===true){
                  $("#taskadd_form").highlight(function(){window.location.hash = "#tasks";});
                  // window.location.hash = "#deps";
 // function(){   window.location.hash = "#deps";    }
                  }
                  else{ alert("Ошибка внесения в базу!\n"+data.errinfo);}

                },
                error: function(jqXHR, textStatus, errorThrown)
                {

                      alert('Ошибка отправки! Проблемы с сервером или подключением '+textStatus);
                }
            });

        });


}).exit(leavePanel);

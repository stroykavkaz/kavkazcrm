
Path.map("#tasks").to(function(){
  // $("#menu-deps").addClass("active");

  $.ajax({
    dataType: "json",
    url: 'tasks',
    // data: data,
    success: function (data) {
      var template = Handlebars.compile( $('#handlebars-tasks').html() );
      $("#main_container").html(template(data)).hide();
      $("#main_container").fadeIn(200);
   }
  }); 
}).exit(leavePanel);

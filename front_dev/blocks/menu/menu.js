function menu_load() {
  $.ajax({
    dataType: "json",
    url: 'login/status',
    //  data: data,
    success: function (data) {
      var template = Handlebars.compile( $('#handlebars-topnav').html() );
      $("#main_topnav").html(template(data));

      $('#menu .ui.dropdown').dropdown({}); // выпадающее меню админ

      $('#main_topnav .ui.search')
      .search({
        apiSettings: {
          url: 'search/{query}'
        },
        fields: {
          results : 'firms',
          title   : 'name_short',
          url     : 'goto_url'
        },     minCharacters : 3
      });

   }
  });

}

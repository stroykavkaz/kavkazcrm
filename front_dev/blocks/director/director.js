
Path.map("#director").to(function(){
    // $("#menu-tasksusers").addClass("active");
  $("#menu-director").addClass("active"); //подсвечиваем в главном меню
  $.ajax({
    dataType: "json",
    url: 'director',
    // data: data,
    success: function (data) {
      var template = Handlebars.compile( $('#handlebars-director').html() );
      $("#main_container").html(template(data)).hide();
      $("#main_container").fadeIn(200);

      $('.my_popup').popup();

      $('.ui.accordion').accordion();
      
    }
  });
}).exit(leavePanel);

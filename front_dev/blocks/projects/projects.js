
Path.map("#projects").to(function(){
  // $("#menu-deps").addClass("active");

  $.ajax({
    dataType: "json",
    url: 'project',
    // data: data,
    success: function (data) {
      var template = Handlebars.compile( $('#handlebars-projects').html() );
      $("#main_container").html(template(data)).hide();
      $("#main_container").fadeIn(200);
   }
  });
}).exit(leavePanel);

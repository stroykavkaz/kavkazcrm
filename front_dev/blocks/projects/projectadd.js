
Path.map("#project/add").to(function(){

      var template = Handlebars.compile( $('#handlebars-projectadd').html() );
      $("#main_container").html(template('')).hide();
      $("#main_container").fadeIn(200);


        $("#projectadd_form").submit(function(e)
        {
          e.preventDefault(); //STOP default action
          var postData = $("#projectadd_form").serializeArray();


            $.ajax(
            {
                type: "POST",
                  url : 'project',
                data: postData,
                success:function(data, textStatus, jqXHR)
                {
// console.log(data);
                  if(data.success===true){
                  $("#projectadd_form").highlight(function(){window.location.hash = "#projects";});

                  }
                  else{
                    // console.log(data);
                     alert("Ошибка внесения в базу!\n"+data.errinfo);}

                },
                error: function(jqXHR, textStatus, errorThrown)
                {

                      alert('Ошибка отправки! Проблемы с сервером или подключением '+textStatus);
                }
            });

        });


}).exit(leavePanel);

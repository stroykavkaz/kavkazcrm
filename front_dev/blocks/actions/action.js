
Path.map("#action/:action_id").to(function(){
    url_params = this.params;
  var org_id;
  $.getJSON('action/'+this.params.action_id, function (data) {
    var template = Handlebars.compile($('#handlebars-actionedit').html());

    $.ajax({
      url: 'tasks',
      dataType: 'json',
      async: false, // // HACK: async is BAD PRACTICE!
      success: function(data1) {
 data.tasks = data1;
          }
    });
console.log(data);

    $("#main_container").html(template(data)).hide();
      $("#main_container").fadeIn(300);

      $('.ui.checkbox').checkbox();
        // $('#task_id').dropdown();
        $('#task_id').dropdown('set selected',data.task_id);

      $('.ui.calendar').calendar({   type: 'date', formatter: {
    date: function (date, settings) {
      if (!date) return '';
      var day = date.getDate();
      var month = date.getMonth() + 1;
      var year = date.getFullYear();
      return year + '-' + month + '-' + day;
      }
      }  });


      org_id =  data.org_id;
      $("#action_editform").submit(function(e)
      {
        e.preventDefault(); //STOP default action
        var postData = $("#action_editform").serializeArray();
        $(this).find('input,textarea,button').attr('disabled','disabled');

          console.log(postData);
          $.ajax(
          {
              type: "POST",
                url : 'action/'+url_params.action_id,
              data: postData,
              success:function(data, textStatus, jqXHR)
              {

                if(data.success===true){
                  window.location.hash = "#firms/"+org_id+"/item";
                }
                else{ alert("Ошибка внесения в базу!\n"+data.errinfo);}

              },
              error: function(jqXHR, textStatus, errorThrown)
              {

                    alert('Ошибка отправки! Проблемы с сервером или подключением '+textStatus);
              }
          });

      });

 });
}).exit(leavePanel);

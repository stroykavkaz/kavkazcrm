
Path.map("#users").to(function(){
  // $("#menu-users").addClass("active");

  $.ajax({
    dataType: "json",
    url: 'users',
    // data: data,
    success: function (data) {
      var template = Handlebars.compile( $('#handlebars-users').html() );
      $("#main_container").html(template(data)).hide();
      $("#main_container").fadeIn(200);
   }
  });
}).exit(leavePanel);


Path.map("#useradd").to(function(){

  $.ajax({
    dataType: "json",
    url: 'deps',
    // data: data,
    success: function (data) {
      var template = Handlebars.compile( $('#handlebars-useradd').html() );
      $("#main_container").html(template(data)).hide(); // прячем чтоб потом показать с fadeIn()
      $("#main_container").fadeIn(200);

        $('select.dropdown').dropdown();
        $('.ui.checkbox').checkbox();

        $("#useradd_form").submit(function(e)
        {
          e.preventDefault(); //STOP default action
          var postData = $("#useradd_form").serializeArray();
          $(this).find('input,textarea,button').attr('disabled','disabled');

            console.log(postData);
            $.ajax(
            {
                type: "POST",
                  url : 'user/add',
                data: postData,
                success:function(data, textStatus, jqXHR)
                {

                  if(data.success===true){
                  $("#useradd_form").highlight( function(){
                    window.location.hash = "#users";
                  });

                  }
                  else{ alert("Ошибка внесения в базу!\n"+data.errinfo);}

                },
                error: function(jqXHR, textStatus, errorThrown)
                {

                      alert('Ошибка отправки! Проблемы с сервером или подключением '+textStatus);
                }
            });

        });

   }
  });


}).exit(leavePanel);

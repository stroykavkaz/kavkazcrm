
Path.map("#user/:user_id").to(function(){
   url_params = this.params;

$.ajax({
  dataType: "json",
  url: 'user/'+url_params.user_id,
  // data: data,
  success: function (data) {
    var template = Handlebars.compile( $('#handlebars-user1').html() );
    $("#main_container").html(template(data)).hide(); // прячем чтоб потом показать с fadeIn()
    $("#main_container").fadeIn(200);
 
      $('.ui.checkbox').checkbox();
      // $('select.dropdown').dropdown();
      $('select.dropdown').popup({on:'focus'});
   $('.my_popup').popup(); // коряво и нерасширяемо. потом подумать

    $("#useredit_form").submit(function(e)
    {
      e.preventDefault(); //STOP default action
      var postData = $("#useredit_form").serializeArray();
      $(this).find('input,textarea,button').attr('disabled','disabled');

        console.log(postData);
        $.ajax(
        {
            type: "POST",
              url : 'user/'+url_params.user_id+'/edit',
            data: postData,
            success:function(data, textStatus, jqXHR)
            {

              if(data.success===true){
              $("#useredit_form").highlight( function(){
                window.location.hash = "#users";
              });

              }
              else{ alert("Ошибка внесения в базу!\n"+data.errinfo);}

            },
            error: function(jqXHR, textStatus, errorThrown)
            {

                  alert('Ошибка отправки! Проблемы с сервером или подключением '+textStatus);
            }
        });

    });

 }
});

}).exit(leavePanel);


function my_favlink() {
  $(this).removeClass('yellow empty grey').addClass('loading');

    if ($(this).data('isinfav')) {

        $.ajax({
          url: 'fav/'+$(this).data('orgid'),
          type: 'DELETE',
          dataType: "json",
            success: function (data) {
              // // HACK: коряво, обращатся надо к точному элементу!
              $('.loading').data('isinfav', 0); // сохраняем в дата-аттрибут значение
              $('.loading').removeClass('loading').addClass('empty grey');
         }
        });


    } else {

      $.ajax({
        url: 'fav/'+$(this).data('orgid'),
        type: 'PUT',
        dataType: "json",
          success: function (data) {
            $('.loading').data('isinfav', 1);
            $('.loading').removeClass('loading').addClass('yellow');
       }
      });

    }

}

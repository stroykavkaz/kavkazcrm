
Path.map("#firms/:firms_id/edit").to(function(){
    url_params = this.params;

  $.getJSON('firms/'+this.params.firms_id+'/item', function (data) {
    var template = Handlebars.compile( $('#handlebars-firmedit').html() );
    $("#main_container").html(template(data)).hide();
      $("#main_container").fadeIn(300);

      $('.ui.checkbox').checkbox();
      $('select.dropdown').dropdown();


      $('#firmedit_form').form({
          onSuccess: function(event, fields) {
              event.preventDefault();
              send_ajax(event);
          },
          message: '.error.message',
          fields: {
              color: {
                  identifier: 'select_dep',
                  rules: [{
                      type: 'empty',
                      prompt: 'Выберите хотя бы одно направление',

                  }]
              }
          }
      });

      // $("#firmedit_form").submit(function(e)
      function send_ajax(e){
        e.preventDefault(); //STOP default action
        var postData = $("#firmedit_form").serializeArray();
        $(this).find('input,textarea,button').attr('disabled','disabled');

          console.log(postData);
          $.ajax(
          {
              type: "POST",
                url : 'firms/'+url_params.firms_id+'/edit',
              data: postData,
              success:function(data, textStatus, jqXHR)
              {

                if(data.success===true){
                $("#firmedit_form").highlight( function(){
                  window.location.hash = "#firms/"+url_params.firms_id+"/item";
                });

                }
                else{ alert("Ошибка внесения в базу!\n"+data.errinfo);}

              },
              error: function(jqXHR, textStatus, errorThrown)
              {

                    alert('Ошибка отправки! Проблемы с сервером или подключением '+textStatus);
              }
          }
        );

      }

 });
}).exit(leavePanel);

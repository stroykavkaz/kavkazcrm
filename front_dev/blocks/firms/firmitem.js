
Path.map("#firms/:firms_id/item").to(function(){
  url_params = this.params;
  // console.log(url_params['firms_id']);
  $.getJSON('firms/'+this.params.firms_id+'/item', function (data,firm_id) {
    var template = Handlebars.compile( $('#handlebars-firmitem').html() );
    $("#main_container").html(template(data)).hide();
      $("#main_container").fadeIn(300);
  $(".my_favlink").click(my_favlink);
      $('.ui.radio.checkbox').checkbox(); // куда-то вынести такие функции, подумать..

      $('#task_id').dropdown();


      $('.ui.calendar').calendar({   type: 'date', formatter: {
    date: function (date, settings) {
      if (!date) return '';
      var day = date.getDate();
      var month = date.getMonth() + 1;
      var year = date.getFullYear();
      return year + '-' + month + '-' + day;
    }
  }  });

      $("#actions_addform").submit(function(e)
      {

        var postData = $("#actions_addform").serializeArray();
        $("#actions_addform").children().attr('disabled','disabled');
        $("#actions_addform").children().children().attr('disabled','disabled');// после дизаблинга из

            console.log(postData);
          $.ajax(
          {
              type: "POST",
                url : 'firms/'+url_params.firms_id+'/item',
              data: postData,
              success:function(data, textStatus, jqXHR)
              {
                $("#actions_addform").children().removeAttr('disabled');
                $("#actions_addform").children().children().removeAttr('disabled');// после


                if(data.success===true){
                  $("#actions_addform")[0].reset(); // сброс формы


                 $(".my_action:first").clone().insertAfter(".my_form").hide(); // клонируем и вставляем элемент

                 // заменяем текст на тексты из джейсона
                 $(".my_action:first .action__date").html(data.datetime1);
                 $(".my_action:first .action__future").html(data.future);
                 $(".my_action:first .action__text").html(data.txt);
                 $(".my_action:first .action__person").html(data.contact_person);
                 $(".my_action:first .action__editicon a").attr('href','#action/'+data.insert_id);

                 $(".my_action:first .action__task").html($("#task_id").find(":selected").text());


                 // посвечиваем
                 $(".my_action:first").slideDown(300,"swing",function(){$(this).highlight();});

                }
                else{ alert("Ошибка внесения в базу!\n"+data.errinfo);
              }

              },
              error: function(jqXHR, textStatus, errorThrown)
              {
                    // alert('Ошибка отправки! Проблемы с сервером или подключением '+textStatus);
                    $("#actions_addform").children().removeAttr('disabled');
                    $("#actions_addform").children().children().removeAttr('disabled');//
              }
          });
          e.preventDefault(); //STOP default action
      });



 }).fail(function( jqxhr, textStatus, error ) {
    $("#main_container").html('<h1>'+error+'</h1><pre>'+jqxhr.responseText+'</pre>');
    // console.log(jqxhr.responseText);
});

}).exit(leavePanel);

Path.map("#firmadd").to(function() {




    $.ajax({
        dataType: "json",
        url: 'deps',
        // data: data,
        success: function(data) {
            window.mydata = 'data';
            window.mydata = data;
            console.log(data);
            var template = Handlebars.compile($('#handlebars-firmadd').html());
            $("#main_container").html(template(data)).hide();
            $("#main_container").fadeIn(300);

            $('.ui .dropdown').dropdown();
            $('.ui.radio.checkbox').checkbox();

            $('#city.ui.search')
                .search({
                    apiSettings: {
                        beforeSend: function(settings) {
                            settings.url = 'city/' + $('#country_id').val() + '/{query}';
                            console.log(settings.url);
                            return settings;
                        }
                    },
                    fields: {
                        results: 'cities',
                        title: 'city',
                        // url     : 'city'
                    },
                    minCharacters: 1,

                });



            $('#firmadd_form').form({
                onSuccess: function(event, fields) {
                    event.preventDefault();
                    send_ajax(event);
                },
                message: '.error.message',
                fields: {
                    color: {
                        identifier: 'select_dep',
                        rules: [{
                            type: 'empty',
                            prompt: 'Выберите хотя бы одно направление',

                        }]
                    }
                }
            });


            function send_ajax(event) {

                var postData = $("#firmadd_form").serializeArray();
                //  $(this).find('input,textarea,button').attr('disabled','disabled');
                //  $(this).children().children().attr('disabled','disabled');// после дизаблинга из формы не сериализируешь данные! мучался с этим пару часов!

                console.log(postData);
                $.ajax({
                    type: "POST",
                    url: 'firms/add',
                    data: postData,
                    success: function(data, textStatus, jqXHR) {
                        $(this).find('input,textarea,button').removeAttr('disabled');

                        if (data.success === true) {

                          // прячем форму и показываем ссылки - карточка и добавить еще
                          function hideaddform(){
                            $("#app-firmadd-responce .js-title").html(data.name_short);
                            $("#app-firmadd-responce .js-goto").attr("href", "#firms/" + data.insert_id + "/item");
                            $("#app-firmadd-responce .js-addonemore").click(function() {
                                $("#app-firmadd-form form").trigger('reset');
                                $("#app-firmadd-responce").slideToggle(300, "swing");
                                $("#app-firmadd-form").slideToggle(500, "swing");
                            });

                            $("#app-firmadd-form").slideToggle(300, "swing");
                            $("#app-firmadd-responce").slideToggle(500, "swing");
                          }

                            $("#firmadd_form").parent().parent().highlight(hideaddform);





                        } else {
                            alert("Ошибка внесения в базу!\n" + data.errinfo);
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert('Ошибка отправки! Проблемы с сервером или подключением ' + textStatus);
                    }
                });
            }
        }

    });

}).exit(leavePanel);

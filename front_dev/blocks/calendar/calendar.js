function calendar_change_url() {
    user_id = ($("#select_user").val()) ? $("#select_user").val() : 'all_users';
    task_id = ($("#select_task").val()) ? $("#select_task").val() : 'all_tasks';

    mydate = $("#calendar_widget").calendar('get date');

    if (mydate) {
        month_num = mydate.getFullYear() + '-' + (mydate.getMonth() + 1) + '-01';
    } else {
      month_num = 'all_months';
    }
    // month_num = (isNaN(mydate.getTime())) ? 'all_months': month_year;



    window.location.hash = "calendar/" + user_id + "/" + task_id + "/" + month_num;

}

function calendar_update_controls_by_hash() {

    var params = window.location.hash.split("/");
    user_id = params[1];
    task_id = params[2];
    month_id = params[3];

    $("#select_user").dropdown('set selected', user_id);
    $("#select_task").dropdown('set selected', task_id);

    if (month_id == 'all_months') {
        $("#calendar_users").val('Все месяцы');
    } else if (month_id == 'cur_month') {
        $("#calendar_users").val('Текущий месяц');
    } else {
        $("#calendar_widget").calendar('set date', (new Date(month_id)));
    }


}


function renderCalendarList(json) {
    var template2 = Handlebars.compile($('#handlebars-calendar-list').html());
    $("#calendar-segments").html(template2(json)).hide();
    $("#calendar-segments").fadeIn(200);
}


Path.map("#calendar(/:user_id)(/:task_id)(/:month)").to(function() {
    // Path.map("#calendar").to(function() {
    url_params = this.params;
    $("#menu-calendar").addClass("active"); //подсвечиваем в главном меню

    $.ajax({
        dataType: "json",
        url: 'calendar/' + url_params.user_id + "/" + url_params.task_id + "/" + url_params.month,
        // data: data,
        success: function(data) {
            window.my_global = data;

            var template = Handlebars.compile($('#handlebars-calendar').html());
            $("#main_container").html(template(data));


            renderCalendarList(data);

            setCssActive();

            // отрисовываем график на третьей вкладке
            // chartist(data.stat.month,data.stat.name,data.stat.name);

            $('#menu_calendar .ui.dropdown').dropdown({
                // onChange: calendar_change_url
            });



            // $('.tabular.menu .item').tab();
            // долго мучался с табами, если просто первому элементу прописать active, то он выводит контент второго, и нужно перещелкнуть на второй, а потом на первый, чтобы все стало нормально.
            //пришлось делать такой хак, причем выделение именно второго таба, выбор 1 таба не работает как надо (такой же глюк как и выше)
            // $('.tabular>.item[data-tab=tab_tasks]').addClass('active');

            $('#calendar_widget').calendar({
                type: 'month',
                onChange: function(date, text) {
                    // console.log(date,text,'calendar_widget_change');
                    // calendar_change_url();

                },
            });

            calendar_update_controls_by_hash();
            $("#calendar_filter_button").click(calendar_change_url);

            // $('#menu_deps .ui.calendar').calendar({
            //     type: 'month',
            //     onChange: function(date, text) {
            //         month = (date.getMonth() + 1) + '-' + date.getFullYear();
            //         // window.location.hash = '#calendar/month/' + month;
            //         ajax_show_actions('dep',$('#select_dep').val(),month);
            //     },
            // });
        }
    });


}).exit(leavePanel);


//OLD графики отключил
// function chartist(month1, labels1, legend1){
//
//
//    legend1 = $.map(legend1, function(value, index) { return [value];});
//
//   var data2 = [];
//  var data1 = month1.forEach(function(item, i, arr) {
//    var arr2 = $.map(item, function(value, index) { return [value];});
//   console.log(arr2);
// data2.push(arr2);
//  });
//
//
//   new Chartist.Line('.ct-chart', {
//     labels: [1,2,3,4,5,6,7,8,9,10,11,12],
//       series:  data2
//   }, {
//       fullWidth: true,
//       chartPadding: {
//           right: 40
//       },
//
//       plugins: [
//             Chartist.plugins.legend({         legendNames:legend1,     }),
//           Chartist.plugins.ctPointLabels({    textAnchor: 'middle'   })
//
//       ]
//   });
//
//
// }



function ajax_show_actions(group_by, subj_id, period) {

    myurl = 'calendar?group_by=' + group_by + '&subj_id=' + subj_id + '&period=' + period;

    $.ajax({
        dataType: "json",
        url: myurl,
        // data: data,
        success: function(data) {
            // chartjs_line();
            // plotjs(data.groupbychart.value, data.groupbychart.labels);
            // chartist(data.stat.month,data.stat.name,data.stat.name);
            // chartist2();

            var template = Handlebars.compile($('#handlebars-calendar-list').html());
            $("#calendar-segments").html(template(data)).hide();
            $("#calendar-segments").fadeIn(200);
        }
    });
}

var gulp = require('gulp');

var sass = require('gulp-sass');
var include = require("gulp-include");
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var count = require('gulp-count');

var webpack = require('gulp-webpack');
// var webpack = require('webpack-stream');
// var order = require('gulp-order');



// https://webref.ru/dev/automate-with-gulp/live-reloading
// http://webformyself.com/gulp-dlya-nachinayushhix/

// соединяем все js по каждому блоку
gulp.task('js', function() {
    return gulp.src('front_dev/blocks/**/*.js')
        // .pipe(concat('main.js'))
        .pipe(count('## js-files selected'))
        // .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        //  .pipe(sourcemaps.write())
        .pipe(gulp.dest('wwwroot/js'));
});

// соединяем все js по каждому блоку
gulp.task('js2', function() {
      return gulp.src('front_dev/app/**/*.js')
      // return gulp.src('front_dev/project/*.js')
        // .pipe(order(['front_dev/app/*/*.js','run_app.js']))
        // .pipe(sourcemaps.init())
        .pipe(count('## js-files selected'))

        .pipe(concat('main2.js'))
        //  .pipe(sourcemaps.write())
        .pipe(gulp.dest('wwwroot/js'));
});

gulp.task('webpack', function() {
  return gulp.src('wwwroot/js/main2.js')
    .pipe(webpack({output: {filename: 'webpacked.js'} }))
    .pipe(gulp.dest('wwwroot/js/'));
});



gulp.task('css', function() {
    return gulp.src('front_dev/app/**/*.css')
        .pipe(concat('main.css'))
        .pipe(gulp.dest('wwwroot/css'));
});


// инклюдим все файлы, используется в app.html для вставки всех handlebar-шаблонов
gulp.task("inc", function() {
    console.log("-- gulp is running task 'inc' - include all in front_dev/templates");

    gulp.src("front_dev/pages/*")
        .pipe(include())
        .on('error', console.log)
        .pipe(gulp.dest("wwwroot/templates"));
});

// основной таск для запуска из командной строки 
gulp.task('watch', function() {
    // gulp.watch('front_dev/**/*.*', ['js','js2','inc',  'css', 'webpack',]);
    gulp.watch('front_dev/**/*.*', ['js2','inc','css']);
});


gulp.task('sass', function() {
    return gulp.src('front_dev/scss/main.scss')
        .pipe(sass()) // Using gulp-sass
        .pipe(gulp.dest('wwwroot/css'));
});

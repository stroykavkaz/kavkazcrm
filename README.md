# Kavkaz CRM

Более-менее удобная web-based CRM 

 


## Back-end

* [Slim PHP micro-framework](https://www.slimframework.com) пока в основном для роутинга

* [Класс ](https://github.com/joshcam/PHP-MySQLi-Database-Class) для работы с MySQL

 
## Front-end

* VueJS (Vuex)

* компоненты Vue находятся в папке front_dev / webApp 

* в папке front_dev / blocks - самая первая версия, еще на jQuery. не используется, только для истории :) 

* SemanticUI
